const express = require('express');
const router = express.Router();
const checktocken = require('../middleware/tokenVerify');
const storyValidation = require('../middleware/story-validation');
const stories = require('../controllers/master/storyController');

router.post('/story/uploadfile', checktocken, stories.uploadFileToStory);
router.post('/story/add', checktocken, stories.addStory);
router.post('/story/addfrompost', checktocken, storyValidation.addStoryFromPostValidation, stories.addStoryFromPostFile);
router.post('/story/addfrompostid', checktocken, storyValidation.addStoryFromPostIdValidation, stories.addStoryFromPostId);
router.put('/story/:id', checktocken, stories.updateStory);
router.delete('/story/:id', checktocken, stories.deleteStory);
router.get('/story/list', checktocken, stories.getStoryLists);
router.get('/story/updateseencount/:id', checktocken, stories.updateSeenCount);
router.get('/allstories', checktocken, stories.getAllStoryLists);
router.get('/storycreateby', stories.changeStoryCreatedBy);

module.exports = router;
