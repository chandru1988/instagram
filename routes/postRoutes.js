const express = require('express');
const router = express.Router();
const checktocken = require('../middleware/tokenVerify');
const postValidation = require('../middleware/post-validation');
const posts = require('../controllers/master/postController');

router.post('/post/add', checktocken, posts.addPost);
router.put('/post/:id', checktocken, posts.updatePost);
router.post('/post/uploadfile', checktocken, posts.uploadFileToPost);
router.post('/post/uploadfiles', checktocken, posts.uploadMultiplePostFiles);
router.get('/post/detail/:id', checktocken, posts.getPostDetailsFromId);
router.delete('/post/:id', checktocken, posts.deletePost);
router.delete('/postfile/:id/:encodedfileurl', checktocken, posts.deleteFile);
router.get('/post/list', checktocken, posts.getPostLists);
router.get('/allposts', checktocken, posts.getAllPostLists);
router.get('/files/:uploadtype/:userid', checktocken, posts.getAllFiles);
router.get('/postfiles/:id', checktocken, posts.getFilesFromPostId);
router.post('/likepost', checktocken, postValidation.addLikeValidation, posts.likePost);
router.post('/unlikepost', checktocken, postValidation.addLikeValidation, posts.unlikePost);
router.get('/likecountupdate/:id', posts.updateLikeCount);
router.get('/trend', checktocken, posts.getAllTrendList);
router.get('/followervideos', checktocken, posts.getFollowerVideosList);
router.get('/resharepost/:id', checktocken, posts.resharePost);
router.post('/hashdatas', checktocken, postValidation.getHashPostListValidation, posts.getHashPostLists);
router.get('/pusdatas', posts.pushcreateby);

/*router.get('/organization', checktocken, organization.organizationList);
router.get('/organization/:id', checktocken, organization.organizationSingle);
router.post('/organization', checktocken, masterValidation.orgValidation, organization.createOrganization);
router.put('/organization/:id', checktocken, masterValidation.orgValidation, organization.updateOrganization);
router.delete('/organization/:id', checktocken, organization.deleteOrganization);


router.get('/department', checktocken, department.departmentList);
router.get('/department/:id', checktocken, department.departmentSingle);
router.post('/department', checktocken, masterValidation.departmentValidation, department.createDepartment);
router.put('/department/:id', checktocken, masterValidation.departmentValidation, department.updateDepartment);
router.delete('/department/:id', checktocken, department.deleteDepartment);*/

module.exports = router;
