const express = require('express');
const router = express.Router();
const checktocken = require('../middleware/tokenVerify');
const followValidation = require('../middleware/follow-validation');
const follow = require('../controllers/master/followController');

router.post('/follow/add', checktocken, followValidation.addFollowValidation, follow.addFollow);
router.get('/follower/list/:userid', checktocken, follow.myFollowersList);
router.get('/follower/count/:userid', checktocken, follow.myFollowersCount);
router.get('/following/list/:userid', checktocken, follow.myFollowingList);
router.get('/following/count/:userid', checktocken, follow.myFollowingCount);
router.delete('/unfollow/:following_userid', checktocken, follow.unFollowUsers);
router.post('/follower/search', checktocken, followValidation.searchFollowerValidation, follow.searchFollowerList);
router.get('/followcountupdate/:userid', follow.updateFollowCount);

module.exports = router;
