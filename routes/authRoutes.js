const express = require('express');
const router = express.Router();
var checktocken = require('../middleware/tokenVerify');
const auth = require('../controllers/authController');
const authValidation = require('../middleware/authentication-validation');
const fileUploder = require('../helpers/fileUploder');

router.get('/', auth.home);
router.post('/testupload', fileUploder.upload.single('myFile'), auth.testupload);
router.get('/emailtest', auth.emailtest);
router.post('/signup', authValidation.signupValidation, auth.signup);
router.post('/login', authValidation.loginValidation, auth.login);
router.post('/activate', authValidation.activateValidation, auth.activate);
router.post('/forgotpassword', authValidation.forgotpasswordValidation, auth.forgotpassword);
router.post('/forgotchangepassword', authValidation.forgotChangePasswordValidation, auth.forgotchangepassword);
router.post('/changepassword', checktocken, authValidation.changePasswordValidation, auth.changepassword);
router.post('/oauthlogin', authValidation.oauthValidation, auth.oauthlogin);
router.post('/updateprofileimage', checktocken, auth.updateprofileimage);
router.post('/updateprofile', checktocken, authValidation.updateProfileValidation, auth.updateprofile);
router.get('/myprofile', checktocken, auth.getmyprofile);
router.get('/userdetail/:userid', checktocken, auth.getuserprofile);

module.exports = router;