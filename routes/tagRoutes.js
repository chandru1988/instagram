const express = require('express');
const router = express.Router();
const checktocken = require('../middleware/tokenVerify');
const tagValidation = require('../middleware/tag-validation');
const tags = require('../controllers/master/tagController');

router.post('/tag/add', checktocken, tagValidation.addTagValidation, tags.addTag);
router.post('/tag/add/multiple', checktocken, tags.addTagMultiple);
router.delete('/tag/:id', checktocken, tags.deleteTag);
router.get('/tag/:id', checktocken, tags.getTagDetail);
router.get('/tagtouser/:taguserid', checktocken, tags.getTagToUserList); //Tagged User
router.get('/tagbyuser/:taguserid', checktocken, tags.getTagbyUserList); //CreatedBy User

module.exports = router;
