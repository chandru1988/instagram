const express = require('express');
const router = express.Router();
const checktocken = require('../middleware/tokenVerify');
const sendemail = require('../controllers/emailController');

router.get('/testemail', sendemail.testEmailSend);
router.get('/send_registeremailotp/:userid', sendemail.sendUserRegisterEmailOTP);
router.get('/send_forgotpassword/:userid', sendemail.sendForgotPasswordEmail);
router.get('/send_welcomeemail/:userid', sendemail.sendWelcomeEmail);

module.exports = router;
