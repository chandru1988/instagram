const express = require('express');
const router = express.Router();
const checktocken = require('../middleware/tokenVerify');
const notiValidation = require('../middleware/notification-validation');
const notiController = require('../controllers/master/notificationController');

router.post('/updateFCMToken',notiValidation.updateFCMTokenValidation, notiController.updateUserFCMToken);
router.post('/testFCMMessage',notiValidation.testFCMMessageValidation, notiController.testFCMMessage);
router.get('/mynotifications',checktocken, notiController.getMyNotificationList);

module.exports = router;
