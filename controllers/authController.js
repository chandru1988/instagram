const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

const mail = require('../config/sendemail');
const config = require('../config/config');
const resmessage = require('../config/response');
var dateTime = require('node-datetime');
User = require('../models/userModel');
Follow = require('../models/followModel');

const saltRounds = config.saltrounds;

exports.home = function (req, res){
	res.send('Welcome to Instagram APP API');
};

exports.testupload = function (req, res){
	console.log(req.file);
};

exports.login = function (req, res){
	var jsonData = req.body;
	
	var email = jsonData.email;
	var password = jsonData.password;
	
	User.find({ email: email}, function (err, users) { 
		if(err)
		{
			resmessage.genResponse(res,true,0,'',err.message,'Connection error');
		}
		else
		{
			if(users.length == 0)
			{
				resmessage.genResponse(res,true,0,'',"",'Email not registered');
			}
			else
			{
				var user = users[0];
				
				bcrypt.compare(password, user.password, function (err, comparetstatus) {											
					if(comparetstatus)
					{
						if(user.status != "verified")
						{
							resmessage.genResponse(res,true,0,'',"",'Account not activated');
						}
						else
						{
							var maketoken = {};
							maketoken["userId"] = user._id;
							
							var date = new Date();
							date.setDate(date.getDate() + 1);
							var datetime = date.getFullYear() + "-"
											+ ((date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1) : (date.getMonth()+1))   + "-" 
											+ (date.getDate().toString().length == 1 ? "0"+date.getDate() : date.getDate()) + " "  
											+ (date.getHours().toString().length == 1 ? "0"+date.getHours() : date.getHours()) + ":"  
											+ (date.getMinutes().toString().length == 1 ? "0"+date.getMinutes() : date.getMinutes()) + ":" 
											+ (date.getSeconds().toString().length == 1 ? "0"+date.getSeconds() : date.getSeconds());														                
							maketoken["validTill"] = datetime;
							
							/*let buff = new Buffer(JSON.stringify(maketoken));
							let base64data = buff.toString('base64');*/
							var token = jwt.sign({ userid: user._id }, config.tokensecret, { expiresIn: config.toekenexpiry });
							
							data = {
									"userToken": token,
									"userid" : user._id,
									"firstname" : user.firstname,
									"lastname" : user.lastname,
									"email" : user.email,
									"mobileno" : user.mobileno,
									"username" : user.username,
									"profileimage" : user.profileimage
								};
								
							resmessage.genResponse(res,false,0,'user',data,'User logged successfully');
						}												
					}
					else
					{
						resmessage.genResponse(res,true,0,'',"Invalid password",'Validation error');
					}
				});
			}
		}
	});						

};

exports.signup = function (req, res){
	var jsonData = req.body;
	
	var email = jsonData.email;
	var password = jsonData.password;
	var firstname = jsonData.firstname;
	var lastname = jsonData.lastname;
	var mobileno = jsonData.mobileno;
	
	var randno = Math.floor(Math.random() * (9999 - 1111 + 1) + 1111)
	var username = email.replace(/^(.+)@(.+)$/g,'$1');
	username = username + '-' + randno;
		
	User.find({ email: email}, function (err, users) { 
		if(err)
		{
			resmessage.genResponse(res,true,0,'',err.message,'Connection error');
		}
		else
		{
			if(users.length > 0)
			{
				resmessage.genResponse(res,true,0,'','','Email already registered');
			}
			else
			{
				
				
				//var otp = Math.floor(Math.random() * (9999 - 1111) + 1111);
				var otp = "9999";
				
				bcrypt.genSalt(10, "a",function (errha, saltvalue) {
					
					bcrypt.hash(password, saltvalue, function (errhaa, hash) {
						
						var userdata = new User();
						userdata.email = email;
						userdata.firstname = firstname;
						userdata.lastname = lastname;
						userdata.username = username;
						userdata.password = hash;
						userdata.salt = saltvalue;
						userdata.mobileno = mobileno;
						userdata.emailOTP = otp;						
						
						userdata.save(function (err) {
							if(err)
							{
								resmessage.genResponse(res,true,0,'',err,'Connection error');
							}
							else
							{
								console.log("Insert Done With Id : " + userdata._id);
								
								data = {
										"userId" : userdata._id									
									};
								
								//Trigger Email
								var optionsAsyncForRegisterEmail = {
							        method: 'GET',
							        url: process.env.PROJECT_BASEURL+"send_registeremailotp/"+userdata._id,
							        timeout: 2000,
								};
								
								const request = require('request');
								request(optionsAsyncForRegisterEmail, function(errorAsyncForNextPage, responseAsyncForNextPage, bodyAsyncForNextPage)
								{
								});
								
								resmessage.genResponse(res,false,0,'',data,'User created successfully');									
							}
						});
					});
				});
			}
		}
	});
	
};

exports.activate = function (req, res){
	var jsonData = req.body;
	
	var email = jsonData.email;
	var emailotp = jsonData.emailotp;
	
	User.find({ email: email}, function (err, users) { 
		if(err)
		{
			resmessage.genResponse(res,true,0,'',err.message,'Connection error');
		}
		else
		{
			if(users.length == 0)
			{
				resmessage.genResponse(res,true,0,'',"",'Email not registered');
			}
			else
			{
				var user = users[0];
				
				if(user.emailOTP != emailotp)
				{
					resmessage.genResponse(res,true,0,'',"",'OTP mismatched');
				}
				
				User.findById(user._id, function (err, updateuser) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
					}
					
					var userUpdate = new User(updateuser);
					userUpdate.status = "verified";
					
					userUpdate.save(function (err) {
						if (err)
						{
							resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
						}
						
						console.log("Updated User Status.");
						
						var token = jwt.sign({ userid: user._id }, config.tokensecret, { expiresIn: config.toekenexpiry });
							
						data = {
								"userToken": token,
								"userid" : user._id,
								"firstname" : user.firstname,
								"lastname" : user.lastname,
								"email" : user.email,
								"mobileno" : user.mobileno,
								"username" : user.username,
								"profileimage" : user.profileimage
							};
							
						//Trigger Email
						var optionsAsyncForRegisterEmail = {
					        method: 'GET',
					        url: process.env.PROJECT_BASEURL+"send_welcomeemail/"+user._id,
					        timeout: 2000,
						};
						
						const request = require('request');
						request(optionsAsyncForRegisterEmail, function(errorAsyncForNextPage, responseAsyncForNextPage, bodyAsyncForNextPage)
						{
						});
							
						resmessage.genResponse(res,false,0,'',data,'User activated successfully');
					});
				});
			}
		}
	});						

};

exports.forgotpassword = function (req, res){
	var jsonData = req.body;
	
	var email = jsonData.email;
	
	User.find({ email: email}, function (err, users) { 
		if(err)
		{
			resmessage.genResponse(res,true,0,'',err.message,'Connection error');
		}
		else
		{
			if(users.length == 0)
			{
				resmessage.genResponse(res,true,0,'','','Email not registered');
			}
			else
			{
				var user = users[0];
				//let tempgenpassword = Math.floor(100000 + Math.random() * 900000)
				let tempgenpassword = "999999"
				
				User.findById(user._id, function (err, updateuser) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
					}
					
					var userUpdate = new User(updateuser);
					userUpdate.temp_password = tempgenpassword;
					
					userUpdate.save(function (err) {
						if (err)
						{
							resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
						}
						
						console.log("Updated User Temp Password.");
						console.log(process.env.PROJECT_NAME);
						
						param = {
							toemail: email,
							content: 'Please use below temporary password to change your password.<br/><br/><br/><br/>Temporary Password : '+tempgenpassword,
							subject: 'Reset Password | '+process.env.PROJECT_NAME,
							heading: 'Reset Your Password Procedure'
						}					
						
						//Trigger Email
						var optionsAsyncForRegisterEmail = {
					        method: 'GET',
					        url: process.env.PROJECT_BASEURL+"send_forgotpassword/"+user._id,
					        timeout: 2000,
						};
						
						const request = require('request');
						request(optionsAsyncForRegisterEmail, function(errorAsyncForNextPage, responseAsyncForNextPage, bodyAsyncForNextPage)
						{
						});
						
						resmessage.genResponse(res,false,0,'','','Temporary password sent to your email id. Please check and update your password.');
					});
				});
			}
		}
	});
};

exports.forgotchangepassword = function (req, res){
	var jsonData = req.body;
	
	var email = jsonData.email;
	var temp_password = jsonData.temp_password;
	var new_password = jsonData.new_password;
	
	User.find({ email: email}, function (err, users) { 
		if(err)
		{
			resmessage.genResponse(res,true,0,'',err.message,'Connection error');
		}
		else
		{
			if(users.length == 0)
			{
				resmessage.genResponse(res,true,0,'','','Email not registered');
			}
			else
			{
				var user = users[0];
				
				if(temp_password != user.temp_password)
				{
					resmessage.genResponse(res,true,0,'','','Temporary password mismatched.');
				}
				
				bcrypt.genSalt(10, "a",function (errha, saltvalue) {
					
					bcrypt.hash(new_password, saltvalue, function (errhaa, hash) {
						
						User.findById(user._id, function (err, updateuser) {
							if (err)
							{
								resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
							}
							
							var userUpdate = new User(updateuser);
							userUpdate.password = hash;
							userUpdate.salt = saltvalue;
							
							userUpdate.save(function (err) {
								if (err)
								{
									resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
								}
								
								console.log("Updated User Password.");											
								
								resmessage.genResponse(res,false,0,'','','User password changed successfully');
							});
						});
					});
				});
			}
		}
	});
	
};

exports.changepassword = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var old_password = jsonData.old_password;
	var new_password = jsonData.new_password;
	console.log("UserId : "+userid);
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{			
			bcrypt.compare(old_password, updateuser.password, function (err, comparetstatus) {											
				if(comparetstatus)
				{
					bcrypt.genSalt(10, "a",function (errha, saltvalue) {
						
						bcrypt.hash(new_password, saltvalue, function (errhaa, hash) {
							
							var d = new Date();
							var curdttime = d.toJSON().slice(0,19).replace('T',':');
							console.log(curdttime);
							
							var userUpdate = new User(updateuser);
							userUpdate.password = hash;
							userUpdate.salt = saltvalue;
							userUpdate.lastloggedin = curdttime;
							
							userUpdate.save(function (err) {
								if (err)
								{
									resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
								}
								
								console.log("Updated User New Password.");											
								
								resmessage.genResponse(res,false,0,'','','User password changed successfully');
							});
							
						});
					});
				}
				else
				{
					resmessage.genResponse(res,true,0,'',"Invalid old password",'Validation error');
				}
			});
		}
	});
};

exports.oauthlogin = function (req, res){
	var jsonData = req.body;
	
	var email = jsonData.email;
	var firstname = jsonData.firstname;
	var oauthid = jsonData.oauthid;
	var lastname = ((jsonData.lastname == null || jsonData.lastname == undefined) ? "" : jsonData.lastname);
	var mobileno = ((jsonData.mobileno == null || jsonData.mobileno == undefined) ? "" : jsonData.mobileno);
	var profileimage = ((jsonData.profileimage == null || jsonData.profileimage == undefined) ? "" : jsonData.profileimage);
	
	User.find({ email: email}, function (err, users) { 
		if(err)
		{
			resmessage.genResponse(res,true,0,'',err.message,'Connection error');
		}
		else
		{
			if(users.length > 0)
			{
				user = users[0];
				
				var token = jwt.sign({ userid: user._id }, config.tokensecret, { expiresIn: config.toekenexpiry });
							
				data = {
						"userToken": token,
						"userid" : user._id,
						"firstname" : user.firstname,
						"lastname" : user.lastname,
						"email" : user.email,
						"mobileno" : user.mobileno,
						"username" : user.username,
						"profileimage" : user.profileimage
					};
					
				resmessage.genResponse(res,false,0,'',data,'User loggedin successfully');
			}
			else
			{
				var randno = Math.floor(Math.random() * (9999 - 1111 + 1) + 1111)
				var username = email.replace(/^(.+)@(.+)$/g,'$1');
				username = username + '-' + randno;
				
				var userdata = new User();
				userdata.oauthid = oauthid;
				userdata.email = email;
				userdata.firstname = firstname;
				userdata.lastname = lastname;
				userdata.mobileno = mobileno;
				userdata.username = username;
				userdata.profileimage = profileimage;
				userdata.status = "verified";
				
				userdata.save(function (err) {
					if(err)
					{
						resmessage.genResponse(res,true,0,'',err,'Connection error');
					}
					else
					{
						console.log("Insert Done With Id : " + userdata._id);
						
						var token = jwt.sign({ userid: userdata._id }, config.tokensecret, { expiresIn: config.toekenexpiry });
							
						data = {
								"userToken": token,
								"userid" : userdata._id,
								"firstname" : firstname,
								"lastname" : lastname,
								"email" : email,
								"mobileno" : mobileno,
								"username" : username,
								"profileimage" : profileimage
							};
						
						//Trigger Email
						var optionsAsyncForRegisterEmail = {
					        method: 'GET',
					        url: process.env.PROJECT_BASEURL+"send_welcomeemail/"+userdata._id,
					        timeout: 2000,
						};
						
						const request = require('request');
						request(optionsAsyncForRegisterEmail, function(errorAsyncForNextPage, responseAsyncForNextPage, bodyAsyncForNextPage)
						{
						});
							
						resmessage.genResponse(res,false,0,'',data,'User created successfully');									
					}
				});
			}
		}
	});
	
};

exports.emailtest = function(req,res){
	param = {
		toemail: 'kashokarun@gmail.com',
		content: 'Test content',
		subject: 'Test email',
		heading: 'Welcome to TracPro'
	}
	mail.sendmail(param,function(result,err){
		if(err){
			resmessage.genResponse(res,true,0,'','','There is some problem in email sending.');
		}
		else{
			resmessage.genResponse(res,false,0,'',result,'Email sent successfully.');
		}
	})
}

exports.updateprofileimage = function (req, res){
	var userid = req.userid;
	var jsonData = req.body;
	
	var multer = require('multer');
	
	var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/profileimages');
        },
        filename: function (req, file, cb) {
        	var tmpfilename = file.originalname;
        	var tmparr = tmpfilename.split(".");
        	var fileext = tmparr[tmparr.length - 1];
        	var timestamp = Math.floor(Date.now() / 1000);
        	//var fname = tmpfilename.replace("."+fileext,"_"+timestamp+"."+fileext);
        	var fname = userid+"_"+timestamp+"_"+tmpfilename;
        	fname = fname.replace(new RegExp(" ", 'g'), "");
        	
            cb(null, fname);
        }
    });

    var upload = multer({ storage: storage }).fields([
        { name: 'profile_image', maxCount: 1 }, 
    ]);
	
	upload(req, res, function(err) {
		if (err) 
		{
            resmessage.genResponse(res,true,0,'',"",'Problem in file upload to local dir.');
        }
		else
		{
			if(req.files['profile_image'] == undefined)
			{
				resmessage.genResponse(res,true,0,'',"",'file missing(profile_image)');
			}
			else
			{
				var profileimage = "";				
				console.log("UserId : "+userid);
				
				User.findById(userid, function (err, updateuser) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
					}
					if(updateuser == null)
					{
						resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
					}
					else
					{
						var filename = req.files['profile_image'][0].filename;
						
						const fs = require('fs');
						const AWS = require('aws-sdk');
						
						const s3 = new AWS.S3({
						    accessKeyId: process.env.ACCESS_KEY_ID,
						    secretAccessKey: process.env.SECRET_ACCESS_KEY
						});
						
						const absoluteFilePath = "uploads/profileimages/"+filename;
						
						fs.readFile(absoluteFilePath, (err, data) => {
							if (err) throw err;
							const params = {
							Bucket: process.env.PROFILE_IMAGE_BUCKET_NAME, // pass your bucket name
								Key: filename, // file will be saved in <folderName> folder
								Body: data
							};
							
							s3.upload(params, function (s3Err, data) {
								if (s3Err)
								{
									resmessage.genResponse(res,true,0,'',"",'S3 Err : '+s3Err);
								}
								else
								{
									var s3_url = data.Location;									
						
									var userUpdate = new User(updateuser);
									userUpdate.profileimage = s3_url;
									
									userUpdate.save(function (err) {
										if (err)
										{
											resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
										}
										
										console.log("Updated User Profile Image.");
										
										resmessage.genResponse(res,false,0,'','','User profile image updated successfully');
									});													
								}
							});
						});
					}
				});
			}
		}
	});	
};

exports.updateprofile = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	
	var firstname = ((jsonData.firstname == undefined || jsonData.firstname == null) ? "" : jsonData.firstname);
	var lastname = ((jsonData.lastname == undefined || jsonData.lastname == null) ? "" : jsonData.lastname);
	var website = ((jsonData.website == undefined || jsonData.website == null) ? "" : jsonData.website);
	var bio = ((jsonData.bio == undefined || jsonData.bio == null) ? "" : jsonData.bio);
	var gender = ((jsonData.gender == undefined || jsonData.gender == null) ? "" : jsonData.gender);
	var username = ((jsonData.username == undefined || jsonData.username == null) ? "" : jsonData.username);
	var location = ((jsonData.location == undefined || jsonData.location == null) ? "" : jsonData.location);
	var mobileno = ((jsonData.mobileno == undefined || jsonData.mobileno == null) ? "" : jsonData.mobileno);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			User.find({'$and' :[{_id: { '$ne': userid }}, {username:username}]}, function (err, chkusers) { 
				if(err)
				{
					resmessage.genResponse(res,true,0,'',"",err);
				}
				else
				{
					console.log(chkusers);
				
					if(chkusers.length > 0)
					{
						resmessage.genResponse(res,true,0,'',"",'Username already exists.');
					}
					else
					{
						var userUpdate = new User(updateuser);
						userUpdate.firstname = firstname;
						userUpdate.lastname = lastname;
						userUpdate.website = website;
						userUpdate.bio = bio;
						userUpdate.gender = gender;
						userUpdate.username = username;
						userUpdate.location = location;
						userUpdate.mobileno = mobileno;
						
						userUpdate.save(function (err) {
							if (err)
							{
								resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
							}
							
							console.log("Updated User Profile.");
							
							resmessage.genResponse(res,false,0,'','','User profile updated successfully');
						});
					}
				}				
			});			
		}
	});
};

exports.getmyprofile = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Follow.count({following_userid:userid} ,function (err, followercount) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				Follow.count({userid:userid} ,function (err, followingcount) {
				
					if (err)
					{
						resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
					}
					
					var cri = {};
					cri["_id"] = updateuser._id;
					cri["userid"] = updateuser._id;
					cri["firstname"] = updateuser.firstname;
					cri["lastname"] = updateuser.lastname;
					cri["email"] = updateuser.email;
					cri["mobileno"] = updateuser.mobileno;
					cri["profileimage"] = updateuser.profileimage;
					cri["username"] = updateuser.username;
					cri["website"] = updateuser.website;
					cri["bio"] = updateuser.bio;
					cri["gender"] = updateuser.gender;
					cri["location"] = updateuser.location;
					cri["followercount"] = followercount;
					cri["followingcount"] = followingcount;
					cri["fcmtoken"] = updateuser.fcmtoken;
					
					resmessage.genResponse(res,false,0,'',cri,'User profile details returned successfully');			
				});				
			});			
		}
	});
};

exports.getuserprofile = function (req, res){
	var loggeduserid = req.userid;
	var userid = req.params.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
			return;
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
			return;
		}
		else
		{
			Follow.count({following_userid:userid} ,function (err, followercount) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
					return;
				}
				
				Follow.count({userid:userid} ,function (err, followingcount) {
				
					if (err)
					{
						resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
						return;
					}
					
					Follow.find({'$and' :[{userid:loggeduserid}, {"following_userid" : userid} ]} ,function (err, chkfolloweddata) {
						if (err)
						{
							resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
							return;
						}
						
						var cri = {};
						cri["_id"] = updateuser._id;
						cri["userid"] = updateuser._id;
						cri["firstname"] = updateuser.firstname;
						cri["lastname"] = updateuser.lastname;
						cri["email"] = updateuser.email;
						cri["mobileno"] = updateuser.mobileno;
						cri["profileimage"] = updateuser.profileimage;
						cri["username"] = updateuser.username;
						cri["website"] = updateuser.website;
						cri["bio"] = updateuser.bio;
						cri["gender"] = updateuser.gender;
						cri["location"] = updateuser.location;
						cri["followercount"] = followercount;
						cri["followingcount"] = followingcount;
						cri["fcmtoken"] = updateuser.fcmtoken;
						var is_followed = "no";
						if(chkfolloweddata.length > 0)
						{
							is_followed = "yes";
						}
						cri["is_followed"] = is_followed;
						
						resmessage.genResponse(res,false,0,'',cri,'User profile details returned successfully');
					});
				});				
			});			
		}
	});
};
