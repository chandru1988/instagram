const config = require('../config/config');
const resmessage = require('../config/response');
var hbs = require('nodemailer-express-handlebars');
var MailConfig = require('../config/email');
var dateTime = require('node-datetime');
User = require('../models/userModel');

exports.testEmailSend = function (req, res){
	console.log("Start Sending Mail");
		
	var smtpTransport = MailConfig.SMTPTransport;
	MailConfig.ViewOption(smtpTransport,hbs,'sendEmailOTP');
	
	let HelperOptions = {
		from: '"Famewal" <'+process.env.SMTP_FROM_EMAIL+'>',
		to: "chandru@saaragh.com",
		subject: 'Famewal - User Verification',
		template: 'sendEmailOTP',
		context: {
			otp:"123456"
		}
	};
	
	smtpTransport.verify((error, success) => {
		if(error) 
		{
			var cri = {output: 'error', message: error};
			console.log(cri);				
			resmessage.genResponse(res,true,0,'',cri,'Err.');
		} 
		else 
		{
			smtpTransport.sendMail(HelperOptions, (error,info) => {
				if(error) {
					var cri = {output: 'error', message: error};
					console.log(cri);	
					resmessage.genResponse(res,true,0,'',cri,'Err.');
				}
				else
				{
					console.log('Email sent: ' + info.response);
					setTimeout(function() {
					    resmessage.genResponse(res,false,0,'','','Sent');
					}, 3000);
				}
			});
		}
	});
};

exports.sendUserRegisterEmailOTP = function (req, res){
	console.log("Start Sending Register Mail");
	
	var userid = req.params.userid;
	
	User.findById(userid, function (err, getuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
			return;
		}
		else
		{
			var smtpTransport = MailConfig.SMTPTransport;
			MailConfig.ViewOption(smtpTransport,hbs,'sendRegisterEmailOTP');
			
			let HelperOptions = {
				from: '"Famewal" <'+process.env.SMTP_FROM_EMAIL+'>',
				to: getuser.email,
				subject: 'Famewal - Registered Successfully. User Verification.',
				template: 'sendRegisterEmailOTP',
				context: {
					header:config.emailheader,
					footer:config.emailfooter,
					otp:getuser.emailOTP,
					name:getuser.firstname+" "+getuser.lastname
				}
			};
			
			smtpTransport.verify((error, success) => {
				if(error) 
				{
					var cri = {output: 'error', message: error};
					console.log(cri);				
					resmessage.genResponse(res,true,0,'',cri,'Err.');
				} 
				else 
				{
					smtpTransport.sendMail(HelperOptions, (error,info) => {
						if(error) {
							var cri = {output: 'error', message: error};
							console.log(cri);	
							resmessage.genResponse(res,true,0,'',cri,'Err.');
						}
						else
						{
							console.log('Email sent: ' + info.response);
							setTimeout(function() {
							    resmessage.genResponse(res,false,0,'','','Sent');
							}, 3000);
						}
					});
				}
			});
		}
	});	
};

exports.sendForgotPasswordEmail = function (req, res){
	console.log("Start Sending Forgot password Mail");
	
	var userid = req.params.userid;
	
	User.findById(userid, function (err, getuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
			return;
		}
		else
		{
			var smtpTransport = MailConfig.SMTPTransport;
			MailConfig.ViewOption(smtpTransport,hbs,'sendForgotpasswordEmail');
			
			let HelperOptions = {
				from: '"Famewal" <'+process.env.SMTP_FROM_EMAIL+'>',
				to: getuser.email,
				subject: 'Famewal - Reset Password.',
				template: 'sendForgotpasswordEmail',
				context: {
					header:config.emailheader,
					footer:config.emailfooter,
					password:getuser.temp_password,
					name:getuser.firstname+" "+getuser.lastname
				}
			};
			
			smtpTransport.verify((error, success) => {
				if(error) 
				{
					var cri = {output: 'error', message: error};
					console.log(cri);				
					resmessage.genResponse(res,true,0,'',cri,'Err.');
				} 
				else 
				{
					smtpTransport.sendMail(HelperOptions, (error,info) => {
						if(error) {
							var cri = {output: 'error', message: error};
							console.log(cri);	
							resmessage.genResponse(res,true,0,'',cri,'Err.');
						}
						else
						{
							console.log('Email sent: ' + info.response);
							setTimeout(function() {
							    resmessage.genResponse(res,false,0,'','','Sent');
							}, 3000);
						}
					});
				}
			});
		}
	});	
};

exports.sendWelcomeEmail = function (req, res){
	console.log("Start Sending Welcome Mail");
	
	var userid = req.params.userid;
	
	User.findById(userid, function (err, getuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
			return;
		}
		else
		{
			var smtpTransport = MailConfig.SMTPTransport;
			MailConfig.ViewOption(smtpTransport,hbs,'sendWelcomeEmail');
			
			let HelperOptions = {
				from: '"Famewal" <'+process.env.SMTP_FROM_EMAIL+'>',
				to: getuser.email,
				subject: 'Welcome to Famewal. Your Account Created.',
				template: 'sendWelcomeEmail',
				context: {
					header:config.emailheader,
					footer:config.emailfooter,
					name:getuser.firstname+" "+getuser.lastname
				}
			};
			
			smtpTransport.verify((error, success) => {
				if(error) 
				{
					var cri = {output: 'error', message: error};
					console.log(cri);				
					resmessage.genResponse(res,true,0,'',cri,'Err.');
				} 
				else 
				{
					smtpTransport.sendMail(HelperOptions, (error,info) => {
						if(error) {
							var cri = {output: 'error', message: error};
							console.log(cri);	
							resmessage.genResponse(res,true,0,'',cri,'Err.');
						}
						else
						{
							console.log('Email sent: ' + info.response);
							setTimeout(function() {
							    resmessage.genResponse(res,false,0,'','','Sent');
							}, 3000);
						}
					});
				}
			});
		}
	});	
};

