const config = require('../../config/config');
const resmessage = require('../../config/response');
var dateTime = require('node-datetime');
User = require('../../models/userModel');
Follow = require('../../models/followModel');
Notification = require('../../models/notificationModel');

notifyController = require('./notificationController');

exports.addFollow = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var following_userid = jsonData.following_userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Follow.find({'$and' :[{userid:userid}, {following_userid:following_userid}]} ,function (err, followdatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(followdatas.length == 0)
				{
					var followdata = new Follow();
					followdata.userid = userid;
					followdata.following_userid = following_userid;
					
					followdata.save(function (err) {
						if(err)
						{
							resmessage.genResponse(res,true,0,'',err,'Connection error');
						}
						else
						{
							console.log("Insert Done With Id : " + followdata._id);							
							
							var notifydata = new Notification();
							notifydata.notificationtype = "follow";
							notifydata.postid = null;
							notifydata.notifyuserid = following_userid;
							notifydata.createdby = userid;
							
							notifydata.save(function (err) {
								notifyController.triggerNotification(notifydata._id);
								updateFollowCountFun(userid);
								updateFollowCountFun(following_userid);
								//Start Aynsc Call For Save Follow Details
								/*const request = require('request');
								
								var url = process.env.PROJECT_BASEURL+"followcountupdate/"+userid;
								var optionsAsync = {
							        method: 'GET',
							        url: url,
							        timeout: 1500,
								};
								
								request(optionsAsync, function(errorAsync, responseAsync, bodyAsync)
								{
								});
								//------------------------------------------------------
								var url = process.env.PROJECT_BASEURL+"followcountupdate/"+following_userid;
								var optionsAsync = {
							        method: 'GET',
							        url: url,
							        timeout: 1500,
								};
								
								request(optionsAsync, function(errorAsync, responseAsync, bodyAsync)
								{
								});*/
								//End Aynsc Call For Save Follow Details
								
								data = {
										"followId" : followdata._id
									};
									
								resmessage.genResponse(res,false,0,'',data,'New following created successfully');
							});							
						}
					});
				}
				else
				{
					resmessage.genResponse(res,true,0,'','','You already follwing this person.');
				}
			});			
		}
	});
};

exports.myFollowersList = function (req, res){
	var loggeduserid = req.userid;
	var userid = req.params.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Follow.find({following_userid:userid} ,function (err, followusers) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(followusers.length == 0)
				{
					resmessage.genResponse(res,false,0,'',[],'No Followers found');
				}
				else
				{
					var userids = [];
					for(var i = 0 ; i < followusers.length ; i++)
					{
						userids.push(followusers[i].userid);
					}
										
					User.find({"_id" : { $in : userids}} ,function (err, usersdata) {
						var retarr = [];
						for(var i = 0 ; i < usersdata.length ; i++)
						{
							var cri = {};
							cri["userid"] = usersdata[i]._id;
							cri["firstname"] = usersdata[i].firstname;
							cri["lastname"] = usersdata[i].lastname;
							cri["email"] = usersdata[i].email;
							cri["profileimage"] = usersdata[i].profileimage;
							cri["username"] = usersdata[i].username;
							cri["follower"] = usersdata[i].follower;
							cri["following"] = usersdata[i].following;
							cri["bio"] = usersdata[i].bio;
							cri["website"] = usersdata[i].website;
							cri["is_followed"] = "yes";
							
							retarr.push(cri);
						}
						
						resmessage.genResponse(res,false,0,'',retarr,'Followers returned successfully');
					});
				}
			})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'createdon': -1});
		}
	});
};

exports.myFollowersCount = function (req, res){
	var loggeduserid = req.userid;
	var userid = req.params.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			resmessage.genResponse(res,false,0,'',{"followercount": updateuser.follower},'Follower count returned successfully');
		}
	});
};

exports.myFollowingList = function (req, res){
	var loggeduserid = req.userid;
	var userid = req.params.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Follow.find({userid:userid} ,function (err, followusers) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(followusers.length == 0)
				{
					resmessage.genResponse(res,false,0,'',[],'No Following people found');
				}
				else
				{
					var userids = [];
					for(var i = 0 ; i < followusers.length ; i++)
					{
						userids.push(followusers[i].following_userid);
					}
					
					Follow.find({'$and' :[{following_userid:userid}, {"userid" : { $in : userids}} ]} ,function (err, chkfolloweddata) {
						
						User.find({"_id" : { $in : userids}} ,function (err, usersdata) {
							
							var retarr = [];
							for(var i = 0 ; i < usersdata.length ; i++)
							{
								var cri = {};
								cri["userid"] = usersdata[i]._id;
								cri["firstname"] = usersdata[i].firstname;
								cri["lastname"] = usersdata[i].lastname;
								cri["email"] = usersdata[i].email;
								cri["profileimage"] = usersdata[i].profileimage;
								cri["username"] = usersdata[i].username;
								cri["follower"] = usersdata[i].follower;
								cri["following"] = usersdata[i].following;
								cri["bio"] = usersdata[i].bio;
								cri["website"] = usersdata[i].website;								
								
								var is_followed = "no";
								for(var j = 0 ; j < chkfolloweddata.length ; j++)
								{
									if(chkfolloweddata[j].userid == cri["userid"])
									{
										is_followed = "yes";
										break;
									}
								}
								cri["is_followed"] = is_followed;
								
								retarr.push(cri);
							}
							
							resmessage.genResponse(res,false,0,'',retarr,'Following people returned successfully');
						});
					});					
				}
			})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'createdon': -1});
		}
	});
};

exports.myFollowingCount = function (req, res){
	var loggeduserid = req.userid;
	var userid = req.params.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			resmessage.genResponse(res,false,0,'',{"followercount": updateuser.following},'Following people count returned successfully');
		}
	});
};

exports.unFollowUsers = function (req, res){
	var userid = req.userid;
	var following_userid = req.params.following_userid;
	
	if(following_userid == undefined || following_userid == null)
	{
		resmessage.genResponse(res,true,0,'',"",'Field missing (following_userid)');
		return;
	}
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Follow.find({'$and' :[{userid:userid}, {following_userid:following_userid}]} ,function (err, followusers) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(followusers.length == 0)
				{
					resmessage.genResponse(res,true,0,'','','Cant unfollow, Not following this person.');
				}
				else
				{
					Follow.deleteMany({'$and' :[{userid:userid}, {following_userid:following_userid}]}, function (err) {
					  	if (err){ 
							resmessage.genResponse(res,true,0,'','','Cant unfollow, '+err);
						}
						else
						{
							//Start Aynsc Call For Save Follow Details
							updateFollowCountFun(userid);
							updateFollowCountFun(following_userid);
							/*const request = require('request');
							
							var url = process.env.PROJECT_BASEURL+"followcountupdate/"+userid;
							var optionsAsync = {
						        method: 'GET',
						        url: url,
						        timeout: 1000,
							};
							
							request(optionsAsync, function(errorAsync, responseAsync, bodyAsync)
							{
							});
							//------------------------------------------------------
							var url = process.env.PROJECT_BASEURL+"followcountupdate/"+following_userid;
							var optionsAsync = {
						        method: 'GET',
						        url: url,
						        timeout: 1000,
							};
							
							request(optionsAsync, function(errorAsync, responseAsync, bodyAsync)
							{
							});*/
							//End Aynsc Call For Save Follow Details
							
							resmessage.genResponse(res,false,0,'','','Unfollowed successfully');
						}
					});					
				}
			});
		}
	});
};

exports.searchFollowerList = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var search_username = ((jsonData.search_username == undefined || jsonData.search_username == null) ? "" : jsonData.search_username);
	
	var numPerPage = ((req.query.perpage == undefined || req.query.perpage == null) ? 25 : req.query.perpage);
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var array_search = [];
			array_search.push({status:"verified"});
			array_search.push({ _id: { $ne: userid } });
			
			if(search_username != "")
			{
				array_search.push({'$or' : [{username: { $regex: '.*' + search_username + '.*' } }, {firstname: { $regex: '.*' + search_username + '.*' } } ]});
			}
			console.log(array_search);
			
			//User.find({'$and' :[{status:"verified"}, {'$or' : [{username: { $regex: '.*' + name + '.*' } }, {firstname: { $regex: '.*' + name + '.*' } } ]}]} ,function (err, userdatas) {
			User.find({'$and' :array_search} ,function (err, userdatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(userdatas.length == 0)
				{
					resmessage.genResponse(res,false,0,'',[],'People list returned successfully');
				}
				else
				{
					var obj = [];
					var userids = [];
					for(var i=0;i<userdatas.length;i++)
					{
						var cri = {};
						cri["_id"] = userdatas[i]._id;
						cri["userid"] = userdatas[i]._id;
						cri["firstname"] = userdatas[i].firstname;
						cri["lastname"] = userdatas[i].lastname;
						cri["email"] = userdatas[i].email;
						cri["mobileno"] = userdatas[i].mobileno;
						cri["profileimage"] = userdatas[i].profileimage;
						cri["username"] = userdatas[i].username;
						cri["website"] = userdatas[i].website;
						cri["bio"] = userdatas[i].bio;
						cri["gender"] = userdatas[i].gender;
						cri["location"] = userdatas[i].location;
						cri["follower"] = userdatas[i].follower;
						cri["following"] = userdatas[i].following;
						cri["is_followed"] = "no";
						
						userids.push(userdatas[i]._id);
						
						obj.push(cri);
					}
					
					Follow.find({'$and' :[{userid:userid}, {"following_userid" : { $in : userids}} ]} ,function (err, chkfolloweddata) {
						for(var i = 0 ; i < chkfolloweddata.length ; i++)
						{
							for(var j = 0 ; j < obj.length ; j++)
							{
								if(chkfolloweddata[i].following_userid == obj[j].userid)
								{
									obj[j].is_followed = "yes";
									break;
								}
							}
						}
						
						resmessage.genResponse(res,false,0,'',obj,'People list returned successfully');
					});					
				}
			})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'username': -1});		
		}
	});
};

exports.updateFollowCount = function (req, res){
	var userid = req.params.userid;
	
	setTimeout(function() {
		Follow.count({following_userid:userid} ,function (err, followercount) {
		
			if (err)
			{
				resmessage.genResponse(res,true,0,'','','Error. '+err);
			}
			
			Follow.count({userid:userid} ,function (err, followingcount) {
			
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Error. '+err);
				}
				
				User.findById(userid, function (err, updateuser) {
					
					var userUpdate = new User(updateuser);
					userUpdate.follower = followercount;
					userUpdate.following = followingcount;
					
					userUpdate.save(function (err) {
						if (err)
						{
							resmessage.genResponse(res,true,0,'','','Error. '+err);
							return;
						}
						
						console.log("Updated User Follow Counts.");
						
						resmessage.genResponse(res,false,0,'','','Follow count updated successfully');
					});	
				});
			});
		});
	}, 3000);	
};

function updateFollowCountFun(userid){
	
	Follow.count({following_userid:userid} ,function (err, followercount) {
	
		if (err)
		{
			console.log("Follow Update Err 1 : "+err);
			return;
		}
		
		Follow.count({userid:userid} ,function (err, followingcount) {
		
			if (err)
			{
				console.log("Follow Update Err 2 : "+err);
				return;
			}
			
			User.findById(userid, function (err, updateuser) {
				
				var userUpdate = new User(updateuser);
				userUpdate.follower = followercount;
				userUpdate.following = followingcount;
				
				userUpdate.save(function (err) {
					if (err)
					{
						console.log("Follow Update Err 3 : "+err);
						return;
					}
					
					console.log("Updated User Follow Counts.");
				});	
			});
		});
	});
}