const config = require('../../config/config');
const resmessage = require('../../config/response');
var dateTime = require('node-datetime');
User = require('../../models/userModel');
Story = require('../../models/storyModel');
var moment = require('moment')

exports.addStory = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var message = ((jsonData.message == undefined || jsonData.message == null) ? "" : jsonData.message);
	var filetype = ((jsonData.filetype == undefined || jsonData.filetype == null) ? "" : jsonData.filetype);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{			
			var storydata = new Story();
			storydata.message = message;
			storydata.filetype = filetype;
			storydata.createdby = userid;
			
			storydata.save(function (err) {
				if(err)
				{
					resmessage.genResponse(res,true,0,'',err,'Connection error');
				}
				else
				{
					console.log("Insert Done With Id : " + storydata._id);
					
					data = {
							"storyId" : storydata._id									
						};
						
					resmessage.genResponse(res,false,0,'',data,'Story created successfully');
				}
			});
		}
	});
};

exports.addStoryFromPostFile = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var message = ((jsonData.message == undefined || jsonData.message == null) ? "" : jsonData.message);
	var filetype = ((jsonData.filetype == undefined || jsonData.filetype == null) ? "" : jsonData.filetype);
	var filepath = ((jsonData.filepath == undefined || jsonData.filepath == null) ? "" : jsonData.filepath);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{			
			var storydata = new Story();
			storydata.message = message;
			storydata.filetype = filetype;
			storydata.filepath = filepath;
			storydata.createdby = userid;
			
			storydata.save(function (err) {
				if(err)
				{
					resmessage.genResponse(res,true,0,'',err,'Connection error');
				}
				else
				{
					console.log("Insert Done With Id : " + storydata._id);
					
					data = {
							"storyId" : storydata._id									
						};
						
					resmessage.genResponse(res,false,0,'',data,'Story created successfully');
				}
			});
		}
	});
};

exports.addStoryFromPostId = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var message = ((jsonData.message == undefined || jsonData.message == null) ? "" : jsonData.message);
	var filetype = ((jsonData.filetype == undefined || jsonData.filetype == null) ? "" : jsonData.filetype);
	var postid = jsonData.postid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{			
			var storydata = new Story();
			storydata.message = message;
			storydata.filetype = filetype;
			storydata.filepath = "";
			storydata.postid = postid;
			storydata.createdby = userid;
			
			storydata.save(function (err) {
				if(err)
				{
					resmessage.genResponse(res,true,0,'',err,'Connection error');
				}
				else
				{
					console.log("Insert Done With Id : " + storydata._id);
					
					data = {
							"storyId" : storydata._id									
						};
						
					resmessage.genResponse(res,false,0,'',data,'Story created successfully');
				}
			});
		}
	});
};

exports.uploadFileToStory = function (req, res){
	var userid = req.userid;	
	
	var multer = require('multer');
	
	var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/storyfiles');
        },
        filename: function (req, file, cb) {
        	var tmpfilename = file.originalname;
        	var tmparr = tmpfilename.split(".");
        	var fileext = tmparr[tmparr.length - 1];
        	var timestamp = Math.floor(Date.now() / 1000);
        	//var fname = tmpfilename.replace("."+fileext,"_"+timestamp+"."+fileext);
        	var fname = userid+"_"+timestamp+"_"+tmpfilename;
        	fname = fname.replace(new RegExp(" ", 'g'), "");
        	
            cb(null, fname);
        }
    });

    var upload = multer({ storage: storage }).fields([
        { name: 'story_file', maxCount: 1 }, 
    ]);
	
	upload(req, res, function(err) {
		if (err) 
		{
            resmessage.genResponse(res,true,0,'',"",'Problem in file upload to local dir.');
        }
		else
		{
			if(req.files['story_file'] == undefined)
			{
				resmessage.genResponse(res,true,0,'',"",'file missing(story_file)');
			}
			else
			{
				var jsonData = req.body;	
				var storyid = jsonData.storyid;
				
				if(storyid == "" || storyid == undefined || storyid == null)
				{
					resmessage.genResponse(res,true,0,'',"",'Field missing(storyid).');
					return;
				}
				
				console.log("storyid : "+storyid);
				console.log("UserId : "+userid);
				
				Story.findById(storyid, function (err, storydatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
					}
					
					var filename = req.files['story_file'][0].filename;
						
					const fs = require('fs');
					const AWS = require('aws-sdk');
					
					const s3 = new AWS.S3({
					    accessKeyId: process.env.ACCESS_KEY_ID,
					    secretAccessKey: process.env.SECRET_ACCESS_KEY
					});
					
					const absoluteFilePath = "uploads/storyfiles/"+filename;
					
					fs.readFile(absoluteFilePath, (err, data) => {
						if (err) throw err;
						const params = {
						Bucket: process.env.STORY_FILES_BUCKET_NAME, // pass your bucket name
							Key: filename, // file will be saved in <folderName> folder
							Body: data
						};
						
						s3.upload(params, function (s3Err, data) {
							if (s3Err)
							{
								resmessage.genResponse(res,true,0,'',"",'S3 Err : '+s3Err);
							}
							else
							{
								var s3_url = data.Location;								
					
								var storyUpdate = new Story(storydatas);
								storyUpdate.filepath = s3_url;
								
								storyUpdate.save(function (err) {
									if (err)
									{
										resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
									}
									else
									{
										console.log("Updated Story File.");									
										resmessage.genResponse(res,false,0,'','','Story file updated successfully');
									}									
								});													
							}
						});
					});
				});
			}
		}
	});	
};

exports.updateStory = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var message = ((jsonData.message == undefined || jsonData.message == null) ? "" : jsonData.message);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var storyid = req.params.id;
			console.log("storyid : "+ storyid);
			if(storyid == "" || storyid == undefined || storyid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(storyid).');
				return;
			}
			
			Story.findById(storyid, function (err, storydatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				}
				
				var storyUpdate = new Story(storydatas);
				storyUpdate.message = message;
				
				storyUpdate.save(function (err) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
					}
					else
					{
						console.log("Story Updated.");									
						resmessage.genResponse(res,false,0,'','','Story updated successfully');
					}									
				});	
			});
		}
	});
};

exports.deleteStory = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var storyid = req.params.id;
			console.log("storyid : "+ storyid);
			if(storyid == "" || storyid == undefined || storyid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(storyid).');
				return;
			}
			
			Story.findById(storyid, function (err, storydatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				}
				
				var storyUpdate = new Story(storydatas);
				storyUpdate.status = "delete";
				
				storyUpdate.save(function (err) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
					}
					else
					{
						console.log("Story Deleted.");
						resmessage.genResponse(res,false,0,'','','Story deleted successfully');
					}									
				});	
			});
		}
	});
};

exports.getStoryLists = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var enddate = moment().format('YYYY-MM-DDTHH:mm:ss');
			console.log("enddate : "+enddate);
			var startdate = moment().add(-1, 'days');
			startdate = moment(startdate).format('YYYY-MM-DDTHH:mm:ss');
			console.log("startdate : "+startdate);
			
			Story.find({'$and' :[{status:"active"}, {createdby:userid},{createdon: {$gte: startdate, $lte: enddate}}]})
			.populate({path: 'postid', select: 'message location posttype sharetype filepath'})
			.exec(function (err, storydatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(storydatas.length == 0)
				{
					resmessage.genResponse(res,false,0,'',[],'Stories returned successfully');
				}
				else
				{
					resmessage.genResponse(res,false,0,'',storydatas,'Stories returned successfully');
				}					
			});
		}
	});
};

exports.updateSeenCount = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var storyid = req.params.id;
			console.log("storyid : "+ storyid);
			if(storyid == "" || storyid == undefined || storyid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(storyid).');
				return;
			}
			
			Story.findById(storyid, function (err, storydatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				}
				
				var storyUpdate = new Story(storydatas);
				storyUpdate.seen = parseInt(storydatas.seen) + 1;
				
				storyUpdate.save(function (err) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
					}
					else
					{
						console.log("Story Seen Count Updated.");									
						resmessage.genResponse(res,false,0,'','','Story seen count updated successfully');
					}									
				});	
			});
		}
	});
};

exports.getAllStoryLists = function (req, res){
	var userid = req.userid;
	var following_userid = ((req.query.following_userid == undefined || req.query.following_userid == null) ? "" : req.query.following_userid);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			//Follow.find({userid:userid} ,function (err, followusers) {
			Follow.find({'$and' :[{userid:userid}, {following_userid:{ $ne: userid }} ]} ,function (err, followusers) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				var userids = [];
				if(following_userid == "")
				{
					for(var i = 0 ; i < followusers.length ; i++)
					{
						userids.push(followusers[i].following_userid);
					}
				}
				else
				{
					userids.push(following_userid);
				}
				
				var enddate = moment().format('YYYY-MM-DDTHH:mm:ss');
				console.log(enddate);
				var startdate = moment().add(-1, 'days');
				startdate = moment(startdate).format('YYYY-MM-DDTHH:mm:ss');
				
				Story.find({'$and' :[{status:"active"}, {createdby : { $in : userids}}, {createdon: {$gte: startdate, $lte: enddate}} ]})
				.populate({path: 'postid', select: 'message location posttype sharetype filepath',populate: { path: 'hostuser', select: 'firstname lastname email profileimage username'}})
				.exec(function (err, storydatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
					}
					
					if(storydatas.length == 0)
					{
						resmessage.genResponse(res,false,0,'',[],'Stories returned successfully');
					}
					else
					{
						userids = [];
						
						for(var i = 0 ; i < storydatas.length ; i++)
						{
							userids.push(storydatas[i].createdby);
						}
						
						User.find({"_id" : { $in : userids}} ,function (err, usersdata) {
							var userarr = [];
							for(var i = 0 ; i < usersdata.length ; i++)
							{
								var cri = {};
								cri["userid"] = usersdata[i]._id;
								cri["firstname"] = usersdata[i].firstname;
								cri["lastname"] = usersdata[i].lastname;
								cri["email"] = usersdata[i].email;
								cri["profileimage"] = usersdata[i].profileimage;
								cri["username"] = usersdata[i].username;
								cri["follower"] = usersdata[i].follower;
								cri["following"] = usersdata[i].following;
								cri["is_followed"] = "no";
								cri["story_detail"] = [];
								
								userarr.push(cri);
							}
							
							Follow.find({'$and' :[{userid:userid}, {"following_userid" : { $in : userids}} ]} ,function (err, chkfolloweddata) {
								for(var i = 0 ; i < chkfolloweddata.length ; i++)
								{
									for(var j = 0 ; j < userarr.length ; j++)
									{
										if(chkfolloweddata[i].following_userid == userarr[j].userid)
										{
											userarr[j].is_followed = "yes";
											break;
										}
									}
								}
								
								var storyarr = [];
							
								for(var j = 0 ; j < userarr.length ; j++)
								{
									for(var i = 0 ; i < storydatas.length ; i++)
									{
										if(storydatas[i].createdby.toString() == userarr[j].userid)
										{
											var cri = {};
											cri["storyid"] = storydatas[i]._id;
											cri["message"] = storydatas[i].message;
											cri["filetype"] = storydatas[i].filetype;
											cri["filepath"] = storydatas[i].filepath;
											cri["seen"] = storydatas[i].seen;
											cri["postdetails"] = storydatas[i].postid;
											cri["createdon"] = storydatas[i].createdon;
											
											userarr[j].story_detail.push(cri);
											break;
										}
									}
								}
								
								resmessage.genResponse(res,false,0,'',userarr,'Stories returned successfully');
							});
						});		
					}
				});
			});			
		}
	});
};

exports.changeStoryCreatedBy = function (req, res){
	
	Story.find({} ,function (err, storydatas) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
		}
		
		if(storydatas.length == 0)
		{
			resmessage.genResponse(res,false,0,'',[],'Posts returned successfully');
		}
		else
		{
			for(var i = 0 ; i < storydatas.length ; i++)
			{
				var storydata = new Story(storydatas[i]);
				console.log("Id : "+storydatas[i]._id);
				storydata.createdby = storydata.createdbyy;
				
				storydata.save(function (err) {
					
				});
			}
			resmessage.genResponse(res,false,0,'','','Stories returned successfully');	
		}
	});
};