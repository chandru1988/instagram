const config = require('../../config/config');
const resmessage = require('../../config/response');
var dateTime = require('node-datetime');
User = require('../../models/userModel');
Tag = require('../../models/tagModel');

exports.addTag = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var tagfor = ((jsonData.tagfor == undefined || jsonData.tagfor == null) ? "post" : jsonData.tagfor);
	var postid = ((jsonData.postid == undefined || jsonData.postid == null) ? "" : jsonData.postid);
	var postfileid = ((jsonData.postfileid == undefined || jsonData.postfileid == null) ? "" : jsonData.postfileid);
	var taggeduserid = ((jsonData.taggeduserid == undefined || jsonData.taggeduserid == null || jsonData.taggeduserid == "") ? null : jsonData.taggeduserid);
	var taggedtext = ((jsonData.taggedtext == undefined || jsonData.taggedtext == null) ? "" : jsonData.taggedtext);
	var imageposition = ((jsonData.imageposition == undefined || jsonData.imageposition == null) ? "" : jsonData.imageposition);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var tagdata = new Tag();
			tagdata.tagfor = tagfor;
			tagdata.postid = postid;
			tagdata.postfileid = postfileid;
			tagdata.taggeduserid = taggeduserid;
			tagdata.taggedtext = taggedtext;
			tagdata.imageposition = imageposition;
			tagdata.createdby = userid;
			
			tagdata.save(function (err) {
				if(err)
				{
					resmessage.genResponse(res,true,0,'',err,'Connection error');
				}
				else
				{
					console.log("Tag Insert Done With  Id : " + tagdata._id);
					
					data = {
							"tagId" : tagdata._id
						};
						
					resmessage.genResponse(res,false,0,'',data,'Tag created successfully');
				}
			});		
		}
	});
};

exports.addTagMultiple = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	
	if(jsonData.length == 0)
	{
		resmessage.genResponse(res,true,0,'',"",'Input data not valid array');
		return;
	}
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var Promise = require('bluebird');
			
			Promise.each(jsonData, function(item) {
				var tagfor = ((item.tagfor == undefined || item.tagfor == null) ? "post" : item.tagfor);
				var postid = ((item.postid == undefined || item.postid == null) ? "" : item.postid);
				var postfileid = ((item.postfileid == undefined || item.postfileid == null) ? "" : item.postfileid);
				var taggeduserid = ((item.taggeduserid == undefined || item.taggeduserid == null || item.taggeduserid == "") ? null : item.taggeduserid);
				var taggedtext = ((item.taggedtext == undefined || item.taggedtext == null) ? "" : item.taggedtext);
				var imageposition = ((item.imageposition == undefined || item.imageposition == null) ? "" : item.imageposition);
				
				var tagdata = new Tag();
				tagdata.tagfor = tagfor;
				tagdata.postid = postid;
				tagdata.postfileid = postfileid;
				tagdata.taggeduserid = taggeduserid;
				tagdata.taggedtext = taggedtext;
				tagdata.imageposition = imageposition;
				tagdata.createdby = userid;
				
				tagdata.save(function (err) {
				});	
			}).then(function(result) {
			    // This not run
			}).catch(function(rejection) {
			    
			});
			
			resmessage.genResponse(res,false,0,'','','Tags created successfully');				
		}
	});
};

exports.deleteTag = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var tagid = req.params.id;
			console.log("tagid : "+ tagid);
			if(tagid == "" || tagid == undefined || tagid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(tagid).');
				return;
			}
			
			Tag.find({"_id" : tagid , "status" : "active"} ,function (err, tagchkdata) {
				
				if(tagchkdata.length == 0 )
				{
					resmessage.genResponse(res,true,0,'',"",'Invalid data passed Or already tag removed.');
					return;
				}
				
				Tag.findById(tagid, function (err, tagdatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
					}
					
					var tagUpdate = new Tag(tagdatas);
					tagUpdate.status = "delete";
					
					tagUpdate.save(function (err) {
						if (err)
						{
							resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
						}
						else
						{
							console.log("Tag Deleted.");
							resmessage.genResponse(res,false,0,'','','Tag removed successfully');
						}
					});	
				});
			});			
		}
	});
};

exports.getTagDetail = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var tagid = req.params.id;
			console.log("tagid : "+ tagid);
			if(tagid == "" || tagid == undefined || tagid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(tagid).');
				return;
			}
			
			Tag.findById(tagid)
			.populate({path: 'postid', select: 'message location posttype'})
			.populate({path: 'postfileid', select: 'filepath liked createdon'})
			.populate({path: 'taggeduserid', select: 'firstname lastname email'})
			.populate({path: 'createdby', select: 'firstname lastname email'})
			.exec(function (err, tagdatas) {
				if (err)
				{
					console.log(err);
					resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
					return;
				}
				
				resmessage.genResponse(res,false,0,'',tagdatas,'Tag data returned successfully');
			});
		}
	});
};

exports.getTagToUserList = function (req, res){
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var taguserid = req.params.taguserid;
			console.log("taguserid : "+ taguserid);
			if(taguserid == "" || taguserid == undefined || taguserid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(taguserid).');
				return;
			}
			
			Tag.find({taggeduserid : taguserid})
			.populate({path: 'postid', select: 'message location posttype'})
			.populate({path: 'postfileid', select: 'filepath liked createdon'})
			.populate({path: 'taggeduserid', select: 'firstname lastname email'})
			.populate({path: 'createdby', select: 'firstname lastname email'})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'createdon': -1})
			.exec(function (err, tagdatas) {
				if (err)
				{
					console.log(err);
					resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
					return;
				}
				
				resmessage.genResponse(res,false,0,'',tagdatas,'Tag list returned successfully');
			});
		}
	});
};

exports.getTagbyUserList = function (req, res){
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var taguserid = req.params.taguserid;
			console.log("taguserid : "+ taguserid);
			if(taguserid == "" || taguserid == undefined || taguserid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(taguserid).');
				return;
			}
			
			Tag.find({createdby : taguserid})
			.populate({path: 'postid', select: 'message location posttype'})
			.populate({path: 'postfileid', select: 'filepath liked createdon'})
			.populate({path: 'taggeduserid', select: 'firstname lastname email'})
			.populate({path: 'createdby', select: 'firstname lastname email'})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'createdon': -1})
			.exec(function (err, tagdatas) {
				if (err)
				{
					console.log(err);
					resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
					return;
				}
				
				resmessage.genResponse(res,false,0,'',tagdatas,'Tag list returned successfully');
			});
		}
	});
};