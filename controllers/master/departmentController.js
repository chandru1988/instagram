var config = require('../../config/config');

const model = require('../../models/masterModel');
const resmessage = require('../../config/response');
const table = require('../../config/tableConstant');

exports.departmentList = function (req, res){
	let parameters = {
		tablename: table.TABLE_DEPARTMENT,
		where: 'status = "active"',
	};
	model.listModel(parameters).then(result => {
		resmessage.genResponse(res,false,0,'department',result);
	},err=>{
		resmessage.genResponse(res,true,0,'department',err);
	});	
};

exports.departmentSingle = function (req, res){
	console.log(req.params);
	let parameters = {
		tablename: table.TABLE_DEPARTMENT,
		where: 'status = "active" and department_id='+req.params.id,
	};
	model.listModel(parameters).then(result => {
		var retData = {};
		if(result.length > 0)
		{
			retData = result[0];
		}
		resmessage.genResponse(res,false,0,'department',retData);
	},err=>{
		resmessage.genResponse(res,true,0,'department',err);
	});
};

exports.createDepartment = function (req, res){
	let parameters = {
		tablename: table.TABLE_DEPARTMENT,
		where: "status='active' and department_name='"+req.body.department_name+"'",
	};

	let parameters1 = {
		tablename: table.TABLE_DEPARTMENT,
		fields: req.body,
	};
	
	parameters1.fields.created_by = req.user_id;

	model.listModel(parameters).then(result => {
	if(result.length > 0){
		resmessage.genResponse(res,true,4,'department name');
	}
	else
	{
		model.createModel(parameters1).then(result => {
		var retData = {"department_id" : result};
		resmessage.genResponse(res,false,1,'department',retData,'Department created successfully.');
		},err=>{
		resmessage.genResponse(res,true,1,'department',err);
		});	
	}
	},err=>{
		resmessage.genResponse(res,true,1,'department',err);
	})
};

exports.updateDepartment = function (req, res){
	let parameters = {
		tablename: table.TABLE_DEPARTMENT,
		where: "status='active' and department_id != "+req.params.id+" and department_name='"+req.body.department_name+"'",
	};
	let parameters1 = {
		tablename: table.TABLE_DEPARTMENT,
		fields: req.body,
		where: 'department_id='+req.params.id,
		revision: '1',
	};
	parameters1.fields.updated_by = req.user_id;

	model.listModel(parameters).then(result => {
	if(result.length > 0){
		resmessage.genResponse(res,true,4,'department name');
	}
	else{
		model.updateModel(parameters1).then(result => {
		var retData = {"department_id" : req.params.id};
		resmessage.genResponse(res,false,2,'department',retData,'Department updated successfully.');
		},err=>{
		resmessage.genResponse(res,true,2,'department',err);
		});
	}
	},err=>{
		resmessage.genResponse(res,true,2,'department',err);
	});
};

exports.deleteDepartment = function (req, res){
	let parameters = {
		tablename: table.TABLE_DEPARTMENT,
		fields: {
			status: 'inactive'
		},
		where: 'department_id='+req.params.id,
		//return: 'department_id',
	};
	model.updateModel(parameters).then(result => {
		resmessage.genResponse(res,false,3,'organization',result.rows);
	},err=>{
		resmessage.genResponse(res,true,3,'organization',err);
	});	
};