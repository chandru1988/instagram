const config = require('../../config/config');
const resmessage = require('../../config/response');
var dateTime = require('node-datetime');
User = require('../../models/userModel');
Post = require('../../models/postModel');
Postfile = require('../../models/postfileModel');
Follow = require('../../models/followModel');
Story = require('../../models/storyModel');
Postlike = require('../../models/postlikeModel');
Postmessagetag = require('../../models/postmessagetagModel');
Tag = require('../../models/tagModel');
Notification = require('../../models/notificationModel');
Posthash = require('../../models/posthashModel');

notifyController = require('./notificationController');

exports.addPost = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var message = ((jsonData.message == undefined || jsonData.message == null) ? "" : jsonData.message);
	var location = ((jsonData.location == undefined || jsonData.location == null) ? "" : jsonData.location);
	var posttype = ((jsonData.posttype == undefined || jsonData.posttype == null) ? "followers" : jsonData.posttype);
	var tagged_msg_users = ((jsonData.tagged_msg_users == undefined || jsonData.tagged_msg_users == null) ? [] : jsonData.tagged_msg_users);
	var hash_list = ((jsonData.hash_list == undefined || jsonData.hash_list == null) ? [] : jsonData.hash_list);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var postdata = new Post();
			postdata.message = message;
			postdata.location = location;
			postdata.posttype = posttype;
			postdata.createdby = userid;
			postdata.createdbyy = userid;
			postdata.hostuser = userid;
			
			postdata.save(function (err) {
				if(err)
				{
					resmessage.genResponse(res,true,0,'',err,'Connection error');
				}
				else
				{
					console.log("Insert Done With Id : " + postdata._id);
					var postid = postdata._id;
					
					var Promise = require('bluebird');
					
					Promise.each(tagged_msg_users, function(item) {
						var tagdata = new Postmessagetag();
						tagdata.postid = postid;
						tagdata.taggeduserid = item.taggeduserid;
						tagdata.tagkey = item.tagkey;
						tagdata.createdby = userid;
						
						tagdata.save(function (err) {
						});	
					}).then(function(result) {
					    // This not run
					}).catch(function(rejection) {
					    
					});
					
					Promise.each(hash_list, function(itemhash) {
						var hashdata = new Posthash();
						hashdata.postid = postid;
						hashdata.hashvalue = itemhash;
						hashdata.createdby = userid;
						
						hashdata.save(function (err) {
						});	
					}).then(function(result) {
					    // This not run
					}).catch(function(rejection) {
					    
					});
					
					data = {
							"postId" : postid
						};
						
					resmessage.genResponse(res,false,0,'',data,'Post created successfully');
				}
			});
		}
	});
};

exports.uploadFileToPost = function (req, res){
	var userid = req.userid;	
	
	var multer = require('multer');
	
	var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/postfiles');
        },
        filename: function (req, file, cb) {
        	var tmpfilename = file.originalname;
        	var tmparr = tmpfilename.split(".");
        	var fileext = tmparr[tmparr.length - 1];
        	var timestamp = Math.floor(Date.now() / 1000);
        	//var fname = tmpfilename.replace("."+fileext,"_"+timestamp+"."+fileext);
        	var fname = userid+"_"+timestamp+"_"+tmpfilename;
        	fname = fname.replace(new RegExp(" ", 'g'), "");
        	
            cb(null, fname);
        }
    });

    var upload = multer({ storage: storage }).fields([
        { name: 'post_file', maxCount: 1 }, 
    ]);
	
	upload(req, res, function(err) {
		if (err) 
		{
            resmessage.genResponse(res,true,0,'',"",'Problem in file upload to local dir.');
        }
		else
		{
			if(req.files['post_file'] == undefined)
			{
				resmessage.genResponse(res,true,0,'',"",'file missing(post_file)');
			}
			else
			{
				try
				{
					var jsonData = req.body;	
					var postid = jsonData.postid;
					var tags = jsonData.tags;
					
					if(postid == "" || postid == undefined || postid == null)
					{
						resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
						return;
					}
					
					if(tags == "" || tags == undefined || tags == null)
					{
						tags = [];
					}
					else
					{
						tags = JSON.parse(tags);
					}
					
					var filename = req.files['post_file'][0].filename;
							
					const fs = require('fs');
					const AWS = require('aws-sdk');
					
					const s3 = new AWS.S3({
					    accessKeyId: process.env.ACCESS_KEY_ID,
					    secretAccessKey: process.env.SECRET_ACCESS_KEY
					});
					
					const absoluteFilePath = "uploads/postfiles/"+filename;
					
					fs.readFile(absoluteFilePath, (err, data) => {
						if (err) throw err;
						const params = {
						Bucket: process.env.POST_FILES_BUCKET_NAME, // pass your bucket name
							Key: filename, // file will be saved in <folderName> folder
							Body: data
						};
						
						s3.upload(params, function (s3Err, data) {
							if (s3Err)
							{
								resmessage.genResponse(res,true,0,'',"",'S3 Err : '+s3Err);
							}
							else
							{
								var s3_url = data.Location;
								console.log("Upload File : "+s3_url);
								console.log("postid : "+postid);
								console.log("UserId : "+userid);
								
								var postfiledata = new Postfile();
								postfiledata.postid = postid;
								postfiledata.filepath = s3_url;
								postfiledata.createdby = userid;
								
								postfiledata.save(function (err) {
									if(err)
									{
										resmessage.genResponse(res,true,0,'',err,'Connection error');
									}
									else
									{
										console.log("Insert Done With FileId : " + postfiledata._id);									
										
										Post.findById(postid, function (err, postdatas) {
											if (err)
											{
												resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
												return;
											}
											
											var oldFiles = postdatas.files;
											oldFiles.push(s3_url);										
							
											var postUpdate = new Post(postdatas);
											postUpdate.filepath = s3_url;
											postUpdate.files = oldFiles;
											
											postUpdate.save(function (err) {
												if (err)
												{
													resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
												}
												else
												{
													console.log("Post updated.");
													
													var Promise = require('bluebird');
										
													Promise.each(tags, function(item) {
														var tagfor = ((item.tagfor == undefined || item.tagfor == null) ? "post" : item.tagfor);
														var postfileid = postfiledata._id;
														var taggeduserid = ((item.taggeduserid == undefined || item.taggeduserid == null || item.taggeduserid == "") ? null : item.taggeduserid);
														var taggedtext = ((item.taggedtext == undefined || item.taggedtext == null) ? "" : item.taggedtext);
														var imageposition = ((item.imageposition == undefined || item.imageposition == null) ? "" : item.imageposition);
														
														var tagdata = new Tag();
														tagdata.tagfor = tagfor;
														tagdata.postid = postid;
														tagdata.postfileid = postfileid;
														tagdata.taggeduserid = taggeduserid;
														tagdata.taggedtext = taggedtext;
														tagdata.imageposition = imageposition;
														tagdata.createdby = userid;
														
														tagdata.save(function (err) {
														});	
													}).then(function(result) {
													    // This not run
													}).catch(function(rejection) {
													    
													});
																
													resmessage.genResponse(res,false,0,'','','Post image updated successfully');
												}
											});
										});
									}
								});
							}
						});
					});
				}
				catch (err)
				{
					resmessage.genResponse(res,true,0,'','','Err : '+err);
				}								
			}
		}
	});	
};

exports.uploadMultipleFilesToPost = function (req, res){
	var userid = req.userid;	
	
	var multer = require('multer');
	
	var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/postfiles');
        },
        filename: function (req, file, cb) {
        	var tmpfilename = file.originalname;
        	var tmparr = tmpfilename.split(".");
        	var fileext = tmparr[tmparr.length - 1];
        	var timestamp = Math.floor(Date.now() / 1000);
        	//var fname = tmpfilename.replace("."+fileext,"_"+timestamp+"."+fileext);
        	var fname = userid+"_"+timestamp+"_"+tmpfilename;
        	fname = fname.replace(new RegExp(" ", 'g'), "");
        	
            cb(null, fname);
        }
    });

   /* var upload = multer({ storage: storage }).fields([
        { name: 'post_file', maxCount: 1 }, 
    ]);*/
    var upload = multer({ storage: storage }).any('post_file');
	
	upload(req, res, function(err) {
		if (err) 
		{
            resmessage.genResponse(res,true,0,'',"",'Problem in file upload to local dir.');
        }
		else
		{
			try
			{
				var jsonData = req.body;	
				var postid = jsonData.postid;
				var tags = jsonData.tags;
				
				if(postid == "" || postid == undefined || postid == null)
				{
					resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
					return;
				}
				
				if(tags == "" || tags == undefined || tags == null)
				{
					tags = [];
				}
				else
				{
					tags = JSON.parse(tags);
				}
				
				var filelength = req.files.length;
				console.log("filelength : "+ filelength);
				var fileArr = [];
				for(var i = 0 ; i < filelength ; i++)
				{
					fileArr.push({"slno" : (i+1), "filename" : req.files[i].filename , "postfileid" : ""});
				}
				//var filename = req.files['post_file'][0].filename;
				
				resmessage.genResponse(res,false,0,'',fileArr,'Post image updated successfully');
						
				const fs = require('fs');
				const AWS = require('aws-sdk');
				
				const s3 = new AWS.S3({
				    accessKeyId: process.env.ACCESS_KEY_ID,
				    secretAccessKey: process.env.SECRET_ACCESS_KEY
				});
				
				const absoluteFilePath = "uploads/postfiles/"+filename;
				
				fs.readFile(absoluteFilePath, (err, data) => {
					if (err) throw err;
					const params = {
					Bucket: process.env.POST_FILES_BUCKET_NAME, // pass your bucket name
						Key: filename, // file will be saved in <folderName> folder
						Body: data
					};
					
					s3.upload(params, function (s3Err, data) {
						if (s3Err)
						{
							resmessage.genResponse(res,true,0,'',"",'S3 Err : '+s3Err);
						}
						else
						{
							var s3_url = data.Location;
							console.log("Upload File : "+s3_url);
							console.log("postid : "+postid);
							console.log("UserId : "+userid);
							
							var postfiledata = new Postfile();
							postfiledata.postid = postid;
							postfiledata.filepath = s3_url;
							postfiledata.createdby = userid;
							
							postfiledata.save(function (err) {
								if(err)
								{
									resmessage.genResponse(res,true,0,'',err,'Connection error');
								}
								else
								{
									console.log("Insert Done With FileId : " + postfiledata._id);									
									
									Post.findById(postid, function (err, postdatas) {
										if (err)
										{
											resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
											return;
										}
										
										var oldFiles = postdatas.files;
										oldFiles.push(s3_url);										
						
										var postUpdate = new Post(postdatas);
										postUpdate.filepath = s3_url;
										postUpdate.files = oldFiles;
										
										postUpdate.save(function (err) {
											if (err)
											{
												resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
											}
											else
											{
												console.log("Post updated.");
												
												var Promise = require('bluebird');
									
												Promise.each(tags, function(item) {
													var tagfor = ((item.tagfor == undefined || item.tagfor == null) ? "post" : item.tagfor);
													var postfileid = postfiledata._id;
													var taggeduserid = ((item.taggeduserid == undefined || item.taggeduserid == null || item.taggeduserid == "") ? null : item.taggeduserid);
													var taggedtext = ((item.taggedtext == undefined || item.taggedtext == null) ? "" : item.taggedtext);
													var imageposition = ((item.imageposition == undefined || item.imageposition == null) ? "" : item.imageposition);
													
													var tagdata = new Tag();
													tagdata.tagfor = tagfor;
													tagdata.postid = postid;
													tagdata.postfileid = postfileid;
													tagdata.taggeduserid = taggeduserid;
													tagdata.taggedtext = taggedtext;
													tagdata.imageposition = imageposition;
													tagdata.createdby = userid;
													
													tagdata.save(function (err) {
													});	
												}).then(function(result) {
												    // This not run
												}).catch(function(rejection) {
												    
												});
															
												resmessage.genResponse(res,false,0,'','','Post image updated successfully');
											}
										});
									});
								}
							});
						}
					});
				});
			}
			catch (err)
			{
				resmessage.genResponse(res,true,0,'','','Err : '+err);
			}
		}
	});	
};

exports.uploadMultiplePostFiles = function (req, res){
    var userid = req.userid;
	
	console.log("userid : "+userid);	
	
	var multer = require('multer')
	var multerS3 = require('multer-s3')
	const fs = require('fs');
	const AWS = require('aws-sdk');

	const s3 = new AWS.S3({
	    accessKeyId: process.env.ACCESS_KEY_ID,
	    secretAccessKey: process.env.SECRET_ACCESS_KEY
	});

	var upload = multer({
	  storage: multerS3({
	    s3: s3,
	    bucket: process.env.POST_FILES_BUCKET_NAME,
	    metadata: function (req, file, cb) {
	      cb(null, {fieldName: file.fieldname});
	    },
	    key: function (req, file, cb) {
			
			var tmpfilename = file.originalname;
        	var tmparr = tmpfilename.split(".");
        	var fileext = tmparr[tmparr.length - 1];
        	var timestamp = Math.floor(Date.now() / 1000);
        	//var fname = tmpfilename.replace("."+fileext,"_"+timestamp+"."+fileext);
        	var fname = userid+"_"+timestamp+"_"+tmpfilename;
        	fname = fname.replace(new RegExp(" ", 'g'), "");
        	
            
	      	cb(null, fname.toString());
	      	//cb(null, Date.now().toString());
	    }
	  })
	});	
	
	//upload(req, res, function(err) {
	upload.array('post_file', 20)(req, res, function(err) {
					
		var jsonData = req.body;
		var postid = jsonData.postid;
		var tags = jsonData.tags;
		
		if(postid == "" || postid == undefined || postid == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
			return;
		}
		
		if(tags == "" || tags == undefined || tags == null)
		{
			tags = [];
		}
		else
		{
			tags = JSON.parse(tags);
		}
		
		if(req.files.length != tags.length)
		{
			resmessage.genResponse(res,true,0,'',"",'Tag array count mismatched.');
			return;
		}
		
		console.log("Files : "+JSON.stringify(req.files));
		
		var filelength = req.files.length;
		console.log("filelength : "+ filelength);
		var fileArr = [];
		
		for(var i = 0 ; i < filelength ; i++)
		{
			fileArr.push({"slno" : (i+1), "s3url" : req.files[i].location , "tags" : tags[i]});
		}
		//var filename = req.files['post_file'][0].filename;
		
		Post.findById(postid, function (err, postdatas) {
			if (err)
			{
				resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				return;
			}
			
			var postUpdate = new Post(postdatas);
			postUpdate.filepath = fileArr[fileArr.length - 1].s3url;
			
			postUpdate.save(function (err) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
				}
				else
				{
					var Promise = require('bluebird');
										
					Promise.each(fileArr, function(item) {
						var postfiledata = new Postfile();
						postfiledata.postid = postid;
						postfiledata.filepath = item.s3url;
						postfiledata.createdby = userid;
						
						postfiledata.save(function (err) {
							var postfileid = postfiledata._id;
							
							Promise.each(item.tags, function(tagitem) {
								var tagfor = ((tagitem.tagfor == undefined || tagitem.tagfor == null) ? "post" : tagitem.tagfor);
								var taggeduserid = ((tagitem.taggeduserid == undefined || tagitem.taggeduserid == null || tagitem.taggeduserid == "") ? null : tagitem.taggeduserid);
								var taggedtext = ((tagitem.taggedtext == undefined || tagitem.taggedtext == null) ? "" : tagitem.taggedtext);
								var imageposition = ((tagitem.imageposition == undefined || tagitem.imageposition == null) ? "" : tagitem.imageposition);
								
								var tagdata = new Tag();
								tagdata.tagfor = tagfor;
								tagdata.postid = postid;
								tagdata.postfileid = postfileid;
								tagdata.taggeduserid = taggeduserid;
								tagdata.taggedtext = taggedtext;
								tagdata.imageposition = imageposition;
								tagdata.createdby = userid;
								
								tagdata.save(function (err) {
								});	
							}).then(function(tagresult) {
							    // This not run
							}).catch(function(tagrejection) {
							    
							});
						});
					}).then(function(result) {
					    // This not run
					}).catch(function(rejection) {
					    
					});
					
					resmessage.genResponse(res,false,0,'',fileArr,'Post images added successfully');
				}
			});			
		});		
	});	
}

exports.deleteFile = function (req, res){
	var userid = req.userid;
	var postid = req.params.id;
	var encodedfileurl = req.params.encodedfileurl;
	//var fileurl = decodeURIComponent(escape(atob(encodedfileurl)));
	var fileurl = Buffer.from(encodedfileurl, 'base64').toString();
	//alert(btoa(unescape(encodeURIComponent('ABCD')))); JS
	//alert(Buffer.from('Hello World!').toString('base64')); Nodejs
	
	console.log("postid : "+postid);
	console.log("UserId : "+userid);
	console.log("fileurl : "+fileurl);
	
	Post.findById(postid, function (err, postdatas) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		
		var oldFiles = postdatas.files;
		var newFiles = [];
		var newfileurl = '';
		for(var i = 0 ; i < oldFiles.length ; i++)
		{
			if(oldFiles[i] != fileurl)
			{
				newFiles.push(oldFiles[i]);
				newfileurl = oldFiles[i];
			}			
		}	

		var postUpdate = new Post(postdatas);
		postUpdate.filepath = newfileurl;
		postUpdate.files = newFiles;
		
		postUpdate.save(function (err) {
			if (err)
			{
				resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
			}
			else
			{
				Postfile.find({postid:postid , filepath : fileurl }, function (err, postfiledatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
					}
					
					if(postfiledatas.length == 0)
					{
						resmessage.genResponse(res,true,0,'',"",'File not found');
					}
					else
					{
						Postfile.findById(postfiledatas[0]._id, function (err, postfledatas) {
							if (err)
							{
								resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
							}
							
							var postfileUpdate = new Postfile(postfledatas);
							postfileUpdate.status = "delete";
							
							postfileUpdate.save(function (err) {
								if (err)
								{
									resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
								}
								else
								{
									console.log("File Deleted.");										
									resmessage.genResponse(res,false,0,'','','File deleted successfully');
								}
							});	
						});
					}					
				});				
			}
		});
	});	
};

exports.deletePost = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var postid = req.params.id;
			console.log("postid : "+ postid);
			if(postid == "" || postid == undefined || postid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
				return;
			}
			
			Post.findById(postid, function (err, postdatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				}
				
				var postUpdate = new Post(postdatas);
				postUpdate.status = "delete";
				
				postUpdate.save(function (err) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
					}
					else
					{
						console.log("Post Deleted.");
						resmessage.genResponse(res,false,0,'','','Post deleted successfully');
					}
				});	
			});
		}
	});
};

exports.updatePost = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var message = ((jsonData.message == undefined || jsonData.message == null) ? "" : jsonData.message);
	var location = ((jsonData.location == undefined || jsonData.location == null) ? "" : jsonData.location);
	var posttype = ((jsonData.posttype == undefined || jsonData.posttype == null) ? "nil" : jsonData.posttype);
	var tagged_msg_users = ((jsonData.tagged_msg_users == undefined || jsonData.tagged_msg_users == null) ? [] : jsonData.tagged_msg_users);
	var hash_list = ((jsonData.hash_list == undefined || jsonData.hash_list == null) ? [] : jsonData.hash_list);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var postid = req.params.id;
			console.log("postid : "+ postid);
			if(postid == "" || postid == undefined || postid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
				return;
			}
			
			Post.findById(postid, function (err, postdatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				}
				
				var postUpdate = new Post(postdatas);
				postUpdate.message = message;
				postUpdate.location = location;
				if(posttype != "nil")
				{
					postUpdate.posttype = posttype;
				}
				
				postUpdate.save(function (err) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
					}
					else
					{
						console.log("Post Updated.");
						
						Postmessagetag.deleteMany({ postid: postid }, function (err) {
							
							Posthash.deleteMany({ postid: postid }, function (err) {
								
								console.log("Post Message Tags Deleted.");
							
								var Promise = require('bluebird');
						
								Promise.each(tagged_msg_users, function(item) {
									var tagdata = new Postmessagetag();
									tagdata.postid = postid;
									tagdata.taggeduserid = item.taggeduserid;
									tagdata.tagkey = item.tagkey;
									tagdata.createdby = userid;
									
									tagdata.save(function (err) {
									});
								}).then(function(result) {
								    // This not run
								}).catch(function(rejection) {
								    
								});
								
								Promise.each(hash_list, function(itemhash) {
									var hashdata = new Posthash();
									hashdata.postid = postid;
									hashdata.hashvalue = itemhash;
									hashdata.createdby = userid;
									
									hashdata.save(function (err) {
									});	
								}).then(function(result) {
								    // This not run
								}).catch(function(rejection) {
								    
								});
								
								resmessage.genResponse(res,false,0,'','','Post updated successfully');
							});							
						});						
					}									
				});	
			});
		}
	});
};

exports.getPostLists = function (req, res){
	var loggeduserid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	var userid = ((req.query.userid == undefined || req.query.userid == null) ? "" : req.query.userid);
	
	if(userid == '')
	{
		userid = loggeduserid;
	}
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Post.find({'$and' :[{status:"active"}, {createdby:userid}]})
				.populate({path: 'hostuser', select: 'firstname lastname email profileimage username'})
				.limit(numPerPage)
				.skip(numPerPage * page)
				.sort({'createdon': -1})
				.exec(function (err, postdatas) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(postdatas.length == 0)
				{
					resmessage.genResponse(res,false,0,'',[],'Posts returned successfully');
				}
				else
				{
					postids = [];
						
					for(var i = 0 ; i < postdatas.length ; i++)
					{
						postids.push(postdatas[i]._id);
					}					
					
					Postfile.find({"postid" : { $in : postids} , "status" : "active"} ,function (err, postfilesdata) {
					
						Postlike.aggregate([
						  {$match: {"postid" : { $in : postids}}},
						  {$sort: {created: -1}},
						  {$group: {_id:'$postid', title:{$push: '$createdby'}}},
						  {$project: {postid: '$_id', createdUserIds: '$title'}}] ,function (err, reslikesdata) {
						  								
							var likesdata = [];
							var likeduserids = [];
							//console.log("132 : "+JSON.stringify(reslikesdata));
							
							for(var i = 0 ; i < reslikesdata.length ; i++)
							{
								var tmpids = [];
								var createdUserIds = reslikesdata[i].createdUserIds;
								if(createdUserIds.length > 0)
								{
									tmpids.push(createdUserIds[0].toString());
									likeduserids.push(createdUserIds[0].toString())
								}
								if(createdUserIds.length > 1)
								{
									tmpids.push(createdUserIds[1].toString());
									likeduserids.push(createdUserIds[1].toString())
								}								
								
								var lcri = {"postid" : reslikesdata[i].postid, "userids" : tmpids, "userdetails" : []};
								likesdata.push(lcri);
							}
							
							likeduserids = likeduserids.filter((v, k, a) => a.indexOf(v) === k);

							//console.log("LUI : "+likeduserids);
							//console.log("LD : "+JSON.stringify(likesdata));
							
							User.find({"_id" : { $in : likeduserids}} ,function (err, likedusersdata) {
								
								var likesdata_new = [];
								
								for(var k = 0 ; k < likesdata.length ; k++)
								{
									var tmpUsrArr = [];
									var usrIds = likesdata[k].userids;
									
									for(var j = 0 ; j < usrIds.length ; j++)
									{
										for(var i = 0 ; i < likedusersdata.length ; i++)
										{
											if(usrIds[j] == likedusersdata[i]._id)
											{
												var cri = {};
												cri["userid"] = likedusersdata[i]._id;
												cri["firstname"] = likedusersdata[i].firstname;
												cri["lastname"] = likedusersdata[i].lastname;
												cri["username"] = likedusersdata[i].username;
												cri["profileimage"] = likedusersdata[i].profileimage;
												
												tmpUsrArr.push(cri);
												
												if(usrIds.length == tmpUsrArr.length)
												{
													break;
												}
											}
										}
									}
									
									likesdata_new.push({"postid" : likesdata[k].postid, "userids" : likesdata[k].userids, "userdetails" : tmpUsrArr});
								}
								
								//console.log("LD : "+JSON.stringify(likesdata_new));
								
								Postlike.find({"postid" : { $in : postids} , "createdby" : userid} ,function (err, mypostlikesdata) {
									
									Postmessagetag.find({"postid" : { $in : postids}} ,function (err, mypostmessagetagdata) {
										
										Tag.find({"postid" : { $in : postids}} ,function (err, postfiletagdata) {
											
											Posthash.find({"postid" : { $in : postids}} ,function (err, posthashdata) {
												var postarr = [];
									
												for(var i = 0 ; i < postdatas.length ; i++)
												{
													var cri = {};
													cri["_id"] = postdatas[i]._id;
													cri["postid"] = postdatas[i]._id;
													cri["location"] = postdatas[i].location;
													cri["message"] = postdatas[i].message;
													cri["filepath"] = postdatas[i].filepath;
													cri["files"] = postdatas[i].files;
													cri["file_details"] = [];
													cri["liked"] = postdatas[i].liked;
													cri["like_normal"] = postdatas[i].like_normal;
													cri["like_star"] = postdatas[i].like_star;
													cri["like_heart"] = postdatas[i].like_heart;
													cri["favourite"] = postdatas[i].favourite;
													cri["starred"] = postdatas[i].starred;
													cri["shared"] = postdatas[i].shared;
													cri["tagged_msg_users"] = [];
													cri["hash_list"] = [];
													cri["hostuser"] = ((postdatas[i].hostuser == null || postdatas[i].hostuser == undefined) ? {"_id" : "","firstname" : "","lastname" : "","email" : "","profileimage" : "","username" : ""} : postdatas[i].hostuser);;
													
													for(var j = 0 ; j < postfilesdata.length ; j++)
													{
														if(cri["postid"] == (postfilesdata[j].postid).toString())
														{
															var postfileid = postfilesdata[j]._id;
																	
															var tagarr = [];
															for(var jj = 0 ; jj < postfiletagdata.length ; jj++)
															{
																if(postfileid.toString() == postfiletagdata[jj].postfileid.toString())
																{
																	tagarr.push(postfiletagdata[jj]);
																}
															}
															
															var filearra = {"_id" : postfilesdata[j]._id, "filepath" : postfilesdata[j].filepath, "createdon" : postfilesdata[j].createdon, "tags" : tagarr};
															cri["file_details"].push(filearra);
														}
													}
													
													for(var j = 0 ; j < mypostmessagetagdata.length ; j++)
													{
														if(cri["postid"] == (mypostmessagetagdata[j].postid).toString())
														{
															cri["tagged_msg_users"].push(mypostmessagetagdata[j]);
														}
													}
													
													for(var j = 0 ; j < posthashdata.length ; j++)
													{
														if(cri["postid"] == (posthashdata[j].postid).toString())
														{
															cri["hash_list"].push(posthashdata[j]);
														}
													}
													
													cri["liked_users"] = [];
													for(var j = 0 ; j < likesdata_new.length ; j++)
													{
														if(cri["postid"] == (likesdata_new[j].postid).toString())
														{
															cri["liked_users"] = likesdata_new[j].userdetails;
															break;
														}
													}
													
													cri["is_liked"] = 'no';
													cri["liketype"] = '';
													
													for(var j = 0 ; j < mypostlikesdata.length ; j++)
													{
														if(cri["postid"] == (mypostlikesdata[j].postid).toString())
														{
															cri["is_liked"] = 'yes';
															cri["liketype"] = mypostlikesdata[j].liketype;
															break;
														}
													}
													
													postarr.push(cri);
												}
												
												resmessage.genResponse(res,false,0,'',postarr,'Posts returned successfully');
											});											
										});										
									});									
								});								
							});							
						});			
					});
				}					
			});		
		}
	});
};

exports.getAllPostLists = function (req, res){
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	var following_userid = ((req.query.following_userid == undefined || req.query.following_userid == null) ? "" : req.query.following_userid);
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Follow.find({userid:userid} ,function (err, followusers) {
				
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				var userids = [];
				if(following_userid == "")
				{
					for(var i = 0 ; i < followusers.length ; i++)
					{
						userids.push(followusers[i].following_userid);
					}
				}
				else
				{
					userids.push(following_userid);
				}
				userids.push(userid);
				
				Post.find({'$and' :[{status:"active"}, {'$or' :[{posttype:"public"}, {createdby : { $in : userids}} ]} ]})
				.limit(numPerPage)
				.skip(numPerPage * page)
				.sort({'createdon': -1})
				.populate({path: 'createdbyy', select: 'firstname lastname email profileimage username follower following'})
				.populate({path: 'hostuser', select: 'firstname lastname email profileimage username follower following'})
				.exec(function (err, postdatas) {
				//Post.find({'$and' :[{status:"active"}, {'$or' :[{posttype:"public"}, {createdby : { $in : userids}} ]} ]} ,function (err, postdatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
					}
					
					if(postdatas.length == 0)
					{
						resmessage.genResponse(res,false,0,'',[],'Posts returned successfully');
					}
					else
					{
						userids = [];
						postids = [];
						
						for(var i = 0 ; i < postdatas.length ; i++)
						{
							userids.push(postdatas[i].createdbyy._id);
							postids.push(postdatas[i]._id);
						}						
														
						Follow.find({'$and' :[{userid:userid}, {"following_userid" : { $in : userids}} ]} ,function (err, chkfolloweddata) {
															
							Postfile.find({"postid" : { $in : postids} , "status" : "active"} ,function (err, postfilesdata) {
								
								Postlike.aggregate([
								  {$match: {"postid" : { $in : postids}}},
								  {$sort: {created: -1}},
								  {$group: {_id:'$postid', title:{$push: '$createdby'}}},
								  {$project: {postid: '$_id', createdUserIds: '$title'}}] ,function (err, reslikesdata) {
								
									var likesdata = [];
									var likeduserids = [];
									//console.log("132 : "+JSON.stringify(reslikesdata));
									
									for(var i = 0 ; i < reslikesdata.length ; i++)
									{
										var tmpids = [];
										var createdUserIds = reslikesdata[i].createdUserIds;
										if(createdUserIds.length > 0)
										{
											tmpids.push(createdUserIds[0].toString());
											likeduserids.push(createdUserIds[0].toString())
										}
										if(createdUserIds.length > 1)
										{
											tmpids.push(createdUserIds[1].toString());
											likeduserids.push(createdUserIds[1].toString())
										}								
										
										var lcri = {"postid" : reslikesdata[i].postid, "userids" : tmpids, "userdetails" : []};
										likesdata.push(lcri);
									}
									
									likeduserids = likeduserids.filter((v, k, a) => a.indexOf(v) === k);
									
									User.find({"_id" : { $in : likeduserids}} ,function (err, likedusersdata) {
										var likesdata_new = [];
										
										for(var k = 0 ; k < likesdata.length ; k++)
										{
											var tmpUsrArr = [];
											var usrIds = likesdata[k].userids;
											
											for(var j = 0 ; j < usrIds.length ; j++)
											{
												for(var i = 0 ; i < likedusersdata.length ; i++)
												{
													if(usrIds[j] == likedusersdata[i]._id)
													{
														var cri = {};
														cri["userid"] = likedusersdata[i]._id;
														cri["firstname"] = likedusersdata[i].firstname;
														cri["lastname"] = likedusersdata[i].lastname;
														cri["username"] = likedusersdata[i].username;
														cri["profileimage"] = likedusersdata[i].profileimage;
														
														tmpUsrArr.push(cri);
														
														if(usrIds.length == tmpUsrArr.length)
														{
															break;
														}
													}
												}
											}
											
											likesdata_new.push({"postid" : likesdata[k].postid, "userids" : likesdata[k].userids, "userdetails" : tmpUsrArr});
										}
									
										Postlike.find({"postid" : { $in : postids} , "createdby" : userid} ,function (err, mypostlikesdata) {
											
											Postmessagetag.find({"postid" : { $in : postids}} ,function (err, mypostmessagetagdata) {
												
												Tag.find({"postid" : { $in : postids}})
												.populate({path: 'taggeduserid', select: 'firstname lastname email profileimage username'})
												.exec(function (err, postfiletagdata) {
												
													Posthash.find({"postid" : { $in : postids}} ,function (err, posthashdata) {
														var postarr = [];
													
														for(var i = 0 ; i < postdatas.length ; i++)
														{
															var is_followed = "no";
															for(var j = 0 ; j < chkfolloweddata.length ; j++)
															{
																if(chkfolloweddata[j].following_userid == postdatas[i].createdbyy._id)
																{
																	is_followed = "yes";
																	break;
																}
															}
															
															var cri = {};
															cri["postid"] = postdatas[i]._id;
															cri["location"] = postdatas[i].location;
															cri["message"] = postdatas[i].message;
															cri["filepath"] = postdatas[i].filepath;
															cri["files"] = postdatas[i].files;
															cri["file_details"] = [];
															cri["liked"] = postdatas[i].liked;
															cri["like_normal"] = postdatas[i].like_normal;
															cri["like_star"] = postdatas[i].like_star;
															cri["like_heart"] = postdatas[i].like_heart;
															cri["favourite"] = postdatas[i].favourite;
															cri["starred"] = postdatas[i].starred;
															cri["shared"] = postdatas[i].shared;
															cri["hostuser"] = ((postdatas[i].hostuser == null || postdatas[i].hostuser == undefined) ? {"_id" : "","firstname" : "","lastname" : "","email" : "","profileimage" : "","username" : ""} : postdatas[i].hostuser);
															cri["userdetail"] = {
																	"userid":postdatas[i].createdbyy._id,
																	"firstname":postdatas[i].createdbyy.firstname,
																	"lastname":postdatas[i].createdbyy.lastname,
																	"email":postdatas[i].createdbyy.email,
																	"profileimage":postdatas[i].createdbyy.profileimage,
																	"username":postdatas[i].createdbyy.username,
																	"follower":postdatas[i].createdbyy.follower,
																	"following":postdatas[i].createdbyy.following,
																	"is_followed":is_followed
																};
															cri["tagged_msg_users"] = [];
															cri["hash_list"] = [];
															
															for(var j = 0 ; j < postfilesdata.length ; j++)
															{
																if(cri["postid"] == (postfilesdata[j].postid).toString())
																{
																	var postfileid = postfilesdata[j]._id;
																	
																	var tagarr = [];
																	for(var jj = 0 ; jj < postfiletagdata.length ; jj++)
																	{
																		if(postfileid.toString() == postfiletagdata[jj].postfileid.toString())
																		{
																			tagarr.push(postfiletagdata[jj]);
																		}
																	}
																	
																	var filearra = {"_id" : postfilesdata[j]._id, "filepath" : postfilesdata[j].filepath, "createdon" : postfilesdata[j].createdon, "tags" : tagarr};
																	cri["file_details"].push(filearra);
																}
															}
															
															for(var j = 0 ; j < mypostmessagetagdata.length ; j++)
															{
																if(cri["postid"] == (mypostmessagetagdata[j].postid).toString())
																{
																	cri["tagged_msg_users"].push(mypostmessagetagdata[j]);
																}
															}
															
															for(var j = 0 ; j < posthashdata.length ; j++)
															{
																if(cri["postid"] == (posthashdata[j].postid).toString())
																{
																	cri["hash_list"].push(posthashdata[j]);
																}
															}
															
															cri["liked_users"] = [];
															for(var j = 0 ; j < likesdata_new.length ; j++)
															{
																if(cri["postid"] == (likesdata_new[j].postid).toString())
																{
																	cri["liked_users"] = likesdata_new[j].userdetails;
																	break;
																}
															}
															
															cri["is_liked"] = 'no';
															cri["liketype"] = '';
															
															for(var j = 0 ; j < mypostlikesdata.length ; j++)
															{
																if(cri["postid"] == (mypostlikesdata[j].postid).toString())
																{
																	cri["is_liked"] = 'yes';
																	cri["liketype"] = mypostlikesdata[j].liketype;
																	break;
																}
															}
															
															postarr.push(cri);
														}
														
														resmessage.genResponse(res,false,0,'',postarr,'Posts returned successfully');
													});													
												});												
											});												
										});											
									});
								});
							});
						});						
					}
				})
				/*.limit(numPerPage)
				.skip(numPerPage * page)
				.sort({'createdon': -1})*/;
			});			
		}
	});
};

exports.getAllFiles = function (req, res){
	var uploadtype = req.params.uploadtype;
	var fileuserid = req.params.userid;
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	if(uploadtype == "post" || uploadtype == "story")
	{}
	else
	{
		resmessage.genResponse(res,true,0,'',"",'Invalid value passed(uploadtype). Allow values(post/story)');
		return;
	}
	
	User.findById(fileuserid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			if(uploadtype == "post")
			{
				Post.find({'$and' :[{status:"active"}, {createdby:fileuserid}, {filepath: { '$ne': "" }}]})
					.populate({path: 'hostuser', select: 'firstname lastname email profileimage username'})
					.limit(numPerPage)
					.skip(numPerPage * page)
					.sort({'createdon': -1})
					.exec(function (err, postdatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
					}
					
					if(postdatas.length == 0)
					{
						resmessage.genResponse(res,false,0,'',[],'Files returned successfully');
					}
					else
					{
						postids = [];
						
						for(var i = 0 ; i < postdatas.length ; i++)
						{
							postids.push(postdatas[i]._id);
						}
						
						Postfile.find({"postid" : { $in : postids} , "status" : "active"} ,function (err, postfilesdata) {
							
							var retObj = [];
							
							for(var j = 0 ; j < postdatas.length ; j++)
							{
								for(var i = 0 ; i < postfilesdata.length ; i++)
								{
									if((postdatas[j]._id).toString() == (postfilesdata[i].postid).toString())
									{
										var cri = {};
										cri["id"] = (postfilesdata[i].postid).toString();
										cri["fileid"] = (postfilesdata[i]._id).toString();
										cri["filefrom"] = "post";
										cri["filepath"] = postfilesdata[i].filepath;
										cri["createdon"] = postfilesdata[i].createdon;
										cri["hostuser"] = postdatas[j].hostuser;
										
										retObj.push(cri);
									}									
								}
							}							
							
							resmessage.genResponse(res,false,0,'',retObj,'Files returned successfully');
						})
						.sort({'createdon': -1});
					}					
				});
			}
			else if(uploadtype == "story")
			{
				Story.find({'$and' :[{status:"active"}, {createdby:fileuserid}, {filepath: { '$ne': "" }}]} ,function (err, postdatas) {
					if (err)
					{
						resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
					}
					
					if(postdatas.length == 0)
					{
						resmessage.genResponse(res,false,0,'',[],'Files returned successfully');
					}
					else
					{
						var retObj = [];
						for(var i = 0 ; i < postdatas.length ; i++)
						{
							var cri = {};
							cri["id"] = postdatas[i]._id;
							cri["fileid"] = postdatas[i]._id;
							cri["filefrom"] = "story";
							cri["filepath"] = postdatas[i].filepath;
							cri["createdon"] = postdatas[i].createdon;
							
							retObj.push(cri);
						}
						
						resmessage.genResponse(res,false,0,'',retObj,'Files returned successfully');
					}					
				})
				.limit(numPerPage)
				.skip(numPerPage * page)
				.sort({'createdon': -1});
			}
		}
	});
};

exports.getFilesFromPostId = function (req, res){
	var userid = req.userid;
	var postid = req.params.id;
	
	console.log("postid : "+postid);
	console.log("UserId : "+userid);	
	
	Postfile.find({postid : postid, status : 'active'})
	.populate({path: 'createdby', select: 'firstname lastname email'})
	.populate({path: 'postid', select: 'message location posttype'})
	.exec(function (err, postfiledatas) {
		if (err)
		{
			console.log(err);
			resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
			return;
		}
		
		resmessage.genResponse(res,false,0,'',postfiledatas,'Files returned successfully');
	});
};

exports.getPostDetailsFromId = function (req, res){
	var userid = req.userid;
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var postid = req.params.id;
			console.log("postid : "+ postid);
			if(postid == "" || postid == undefined || postid == null)
			{
				resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
				return;
			}
			
			Post.findById(postid)
			.populate({path: 'createdbyy', select: 'firstname lastname email profileimage username'})
			.populate({path: 'hostuser', select: 'firstname lastname email profileimage username'})
			.exec(function (err, postdatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
				}
				
				Postfile.find({"postid" : postid , "status" : "active"} ,function (err, postfilesdata) {
					
					Postmessagetag.find({postid : postid})
					.populate({path: 'taggeduserid', select: 'firstname lastname email profileimage'})
					.exec(function (err, postmessagetagdatas) {
						if (err)
						{
							console.log(err);
							resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
							return;
						}
						
						Posthash.find({"postid" : postid} ,function (err, posthashdata) {
							
							var cri = {};
							cri["_id"] = postdatas._id;
							cri["postid"] = postdatas._id;
							cri["location"] = postdatas.location;
							cri["message"] = postdatas.message;
							cri["sharetype"] = postdatas.sharetype;
							cri["filepath"] = postdatas.filepath;
							cri["files"] = postdatas.files;							
							cri["liked"] = postdatas.liked;
							cri["favourite"] = postdatas.favourite;
							cri["starred"] = postdatas.starred;
							cri["shared"] = postdatas.shared;
							cri["file_details"] = [];
							cri["hash_list"] = posthashdata;
							cri["userdetail"] = postdatas.createdbyy;
							cri["hostuser"] = ((postdatas.hostuser == null || postdatas.hostuser == undefined) ? {"_id" : "","firstname" : "","lastname" : "","email" : "","profileimage" : "","username" : ""} : postdatas.hostuser);
							
							for(var j = 0 ; j < postfilesdata.length ; j++)
							{
								if(cri["postid"] == (postfilesdata[j].postid).toString())
								{
									cri["file_details"].push(postfilesdata[j]);
								}
							}
							
							cri["tagged_msg_users"] = postmessagetagdatas;
							
							var postarr = cri;
							
							resmessage.genResponse(res,false,0,'',postarr,'Post detail returned successfully');
						});						
					});					
				});
			});
		}
	});
};

exports.likePost = function (req, res){
	var userid = req.userid;
	var jsonData = req.body;
	
	var postid = ((jsonData.postid == null || jsonData.postid == undefined) ? "" : jsonData.postid);
	var liketype = ((jsonData.liketype == null || jsonData.liketype == undefined) ? "" : jsonData.liketype);
		
	if(liketype == 'normal' || liketype == 'star' || liketype == 'heart')
	{}
	else
	{
		resmessage.genResponse(res,true,0,'',"",'Invalid value passed (liketype). Allow values are(normal,star,heart)');
		return;
	}
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Postlike.find({"postid" : postid , "createdby" : userid , "liketype" : liketype} ,function (err, postlikedata) {
				
				if(postlikedata.length > 0)
				{
					resmessage.genResponse(res,true,0,'',"",'Already liked.');
					return;
				}
				
				var likedata = new Postlike();
				likedata.postid = postid;
				likedata.liketype = liketype;
				likedata.createdby = userid;
				
				likedata.save(function (err) {
					if(err)
					{
						resmessage.genResponse(res,true,0,'',err,'Connection error');
					}
					else
					{
						Post.findById(postid, function (err, postdata) {
							
							var notifydata = new Notification();
							notifydata.notificationtype = "post-like";
							notifydata.postid = postid;
							notifydata.notifyuserid = postdata.createdby.toString();
							notifydata.createdby = userid;
							
							notifydata.save(function (err) {
								console.log("Postlike Insert Done With  Id : " + likedata._id);
							
								//Start Aynsc Call For Save Like Count Details
								/*const request = require('request');
								
								var url = process.env.PROJECT_BASEURL+"likecountupdate/"+postid;
								var optionsAsync = {
							        method: 'GET',
							        url: url,
							        timeout: 1000,
								};
								
								request(optionsAsync, function(errorAsync, responseAsync, bodyAsync)
								{
								});*/
								updateLikeCountFun(postid);
								notifyController.triggerNotification(notifydata._id);
								//End Aynsc Call For Save Like Count Details
								
								data = {
										"likeId" : likedata._id
									};
									
								resmessage.genResponse(res,false,0,'',data,'Post liked successfully');
							});
						});
					}
				});
			});			
		}
	});
};

exports.unlikePost = function (req, res){
	var userid = req.userid;
	var jsonData = req.body;
	
	var postid = ((jsonData.postid == null || jsonData.postid == undefined) ? "" : jsonData.postid);
	var liketype = ((jsonData.liketype == null || jsonData.liketype == undefined) ? "" : jsonData.liketype);
		
	if(liketype == 'normal' || liketype == 'star' || liketype == 'heart')
	{}
	else
	{
		resmessage.genResponse(res,true,0,'',"",'Invalid value passed (liketype). Allow values are(normal,star,heart)');
		return;
	}
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Postlike.find({"postid" : postid , "createdby" : userid , "liketype" : liketype} ,function (err, postlikedata) {
				
				if(postlikedata.length == 0)
				{
					resmessage.genResponse(res,true,0,'',"",'Cant Unlike. Because its not liked.');
					return;
				}
				
				Postlike.deleteMany({ _id: postlikedata[0]._id }, function (err) {
					
					Post.findById(postid, function (err, postdata) {
							
						var notifydata = new Notification();
						notifydata.notificationtype = "post-unlike";
						notifydata.postid = postid;
						notifydata.notifyuserid = postdata.createdby.toString();
						notifydata.createdby = userid;
						
						notifydata.save(function (err) {
							console.log("Postlike Deleted");
								
							//Start Aynsc Call For Save Like Count Details
							/*const request = require('request');
							
							var url = process.env.PROJECT_BASEURL+"likecountupdate/"+postid;
							var optionsAsync = {
						        method: 'GET',
						        url: url,
						        timeout: 1000,
							};
							
							request(optionsAsync, function(errorAsync, responseAsync, bodyAsync)
							{
							});*/
							updateLikeCountFun(postid);
							notifyController.triggerNotification(notifydata._id);
							//End Aynsc Call For Save Like Count Details
								
							resmessage.genResponse(res,false,0,'','','Post unliked successfully');
						});							
					});					
				});
			});			
		}
	});
};

exports.updateLikeCount = function (req, res){
	var postid = req.params.id;
	
	Postlike.count({postid:postid, liketype : 'normal'} ,function (err, normallikes) {
		
		if (err)
		{
			resmessage.genResponse(res,true,0,'','','Error. '+err);
		}
		
		Postlike.count({postid:postid, liketype : 'star'} ,function (err, starlikes) {
		
			if (err)
			{
				resmessage.genResponse(res,true,0,'','','Error. '+err);
			}
			
			Postlike.count({postid:postid, liketype : 'heart'} ,function (err, heartlikes) {
		
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Error. '+err);
				}
				
				Post.findById(postid, function (err, updatepost) {
					
					var postUpdate = new Post(updatepost);
					postUpdate.like_normal = normallikes;
					postUpdate.like_star = starlikes;
					postUpdate.like_heart = heartlikes;
					postUpdate.liked = parseInt(normallikes + starlikes + heartlikes);
					
					try
					{
						postUpdate.save(function (err) {
							if (err)
							{
								resmessage.genResponse(res,true,0,'','','Error. '+err);
							}
							
							console.log("Updated Post Like Counts.");
							
							resmessage.genResponse(res,false,0,'','','Post likes count updated successfully');
						});
					}
					catch (err)
					{
						console.log("TRY CATCH ERROR : "+err);
						resmessage.genResponse(res,false,0,'','','Post likes count updated successfully');
					}
				});
			});	
		});
	});
};

exports.getAllTrendList = function (req, res){
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;	
	
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			Post.find({'$and' :[{status:"active"}]})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'liked': -1, 'createdon': -1})
			.populate({path: 'createdbyy', select: 'firstname lastname email profileimage username'})
			.exec(function (err, postdatas) {
			//Post.find({'$and' :[{status:"active"}]} ,function (err, postdatas) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				}
				
				if(postdatas.length == 0)
				{
					resmessage.genResponse(res,false,0,'',[],'Posts returned successfully');
				}
				else
				{
					userids = [];
					postids = [];
					
					for(var i = 0 ; i < postdatas.length ; i++)
					{
						userids.push(postdatas[i].createdbyy._id);
						postids.push(postdatas[i]._id);
					}					
						
					Follow.find({'$and' :[{userid:userid}, {"following_userid" : { $in : userids}} ]} ,function (err, chkfolloweddata) {													
						
						Postfile.find({"postid" : { $in : postids} , "status" : "active"} ,function (err, postfilesdata) {
						
							Postmessagetag.find({"postid" : { $in : postids}})
							.populate({path: 'taggeduserid', select: 'firstname lastname email profileimage'})
							.exec(function (err, postmessagetagdatas) {
								if (err)
								{
									console.log(err);
									resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
									return;
								}
								
								Postlike.aggregate([
								  {$match: {"postid" : { $in : postids}}},
								  {$sort: {created: -1}},
								  {$group: {_id:'$postid', title:{$push: '$createdby'}}},
								  {$project: {postid: '$_id', createdUserIds: '$title'}}] ,function (err, reslikesdata) {
								  	
									
									var likesdata = [];
									var likeduserids = [];
									//console.log("132 : "+JSON.stringify(reslikesdata));
									
									for(var i = 0 ; i < reslikesdata.length ; i++)
									{
										var tmpids = [];
										var createdUserIds = reslikesdata[i].createdUserIds;
										if(createdUserIds.length > 0)
										{
											tmpids.push(createdUserIds[0].toString());
											likeduserids.push(createdUserIds[0].toString())
										}
										if(createdUserIds.length > 1)
										{
											tmpids.push(createdUserIds[1].toString());
											likeduserids.push(createdUserIds[1].toString())
										}								
										
										var lcri = {"postid" : reslikesdata[i].postid, "userids" : tmpids, "userdetails" : []};
										likesdata.push(lcri);
									}
									
									likeduserids = likeduserids.filter((v, k, a) => a.indexOf(v) === k);
									
									User.find({"_id" : { $in : likeduserids}} ,function (err, likedusersdata) {
										var likesdata_new = [];
										
										for(var k = 0 ; k < likesdata.length ; k++)
										{
											var tmpUsrArr = [];
											var usrIds = likesdata[k].userids;
											
											for(var j = 0 ; j < usrIds.length ; j++)
											{
												for(var i = 0 ; i < likedusersdata.length ; i++)
												{
													if(usrIds[j] == likedusersdata[i]._id)
													{
														var cri = {};
														cri["userid"] = likedusersdata[i]._id;
														cri["firstname"] = likedusersdata[i].firstname;
														cri["lastname"] = likedusersdata[i].lastname;
														cri["username"] = likedusersdata[i].username;
														cri["profileimage"] = likedusersdata[i].profileimage;
														
														tmpUsrArr.push(cri);
														
														if(usrIds.length == tmpUsrArr.length)
														{
															break;
														}
													}
												}
											}
											
											likesdata_new.push({"postid" : likesdata[k].postid, "userids" : likesdata[k].userids, "userdetails" : tmpUsrArr});
										}
										
										Postlike.find({"postid" : { $in : postids} , "createdby" : userid} ,function (err, mypostlikesdata) {
											
											var postarr = [];
					
											for(var i = 0 ; i < postdatas.length ; i++)
											{
												var is_followed = "no";
												for(var j = 0 ; j < chkfolloweddata.length ; j++)
												{
													if(chkfolloweddata[j].following_userid == postdatas[i].createdbyy._id)
													{
														is_followed = "yes";
														break;
													}
												}
												
												var cri = {};
												cri["postid"] = postdatas[i]._id;
												cri["location"] = postdatas[i].location;
												cri["message"] = postdatas[i].message;
												cri["filepath"] = postdatas[i].filepath;
												cri["file_details"] = [];
												cri["tagged_msg_users"] = [];
												cri["liked"] = postdatas[i].liked;
												cri["like_normal"] = postdatas[i].like_normal;
												cri["like_star"] = postdatas[i].like_star;
												cri["like_heart"] = postdatas[i].like_heart;
												cri["favourite"] = postdatas[i].favourite;
												cri["starred"] = postdatas[i].starred;
												cri["shared"] = postdatas[i].shared;
												cri["userdetail"] = {
													"userid":postdatas[i].createdbyy._id,
													"firstname":postdatas[i].createdbyy.firstname,
													"lastname":postdatas[i].createdbyy.lastname,
													"email":postdatas[i].createdbyy.email,
													"profileimage":postdatas[i].createdbyy.profileimage,
													"username":postdatas[i].createdbyy.username,
													"is_followed":is_followed
												};									
												
												for(var j = 0 ; j < postfilesdata.length ; j++)
												{
													if(cri["postid"] == (postfilesdata[j].postid).toString())
													{
														cri["file_details"].push(postfilesdata[j]);
													}
												}
												
												for(var j = 0 ; j < postmessagetagdatas.length ; j++)
												{
													if(cri["postid"] == (postmessagetagdatas[j].postid).toString())
													{
														cri["tagged_msg_users"].push(postmessagetagdatas[j]);
													}
												}
												
												cri["liked_users"] = [];
												for(var j = 0 ; j < likesdata_new.length ; j++)
												{
													if(cri["postid"] == (likesdata_new[j].postid).toString())
													{
														cri["liked_users"] = likesdata_new[j].userdetails;
														break;
													}
												}
												
												cri["is_liked"] = 'no';
												cri["liketype"] = '';
												
												for(var j = 0 ; j < mypostlikesdata.length ; j++)
												{
													if(cri["postid"] == (mypostlikesdata[j].postid).toString())
													{
														cri["is_liked"] = 'yes';
														cri["liketype"] = mypostlikesdata[j].liketype;
														break;
													}
												}
												
												postarr.push(cri);
											}
											
											resmessage.genResponse(res,false,0,'',postarr,'Posts returned successfully');	
										});																			
									});
								});								
							});
						});
					});							
				}
			})
			/*.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'liked': -1, 'createdon': -1})*/;					
		}
	});
};

exports.getFollowerVideosList = function (req, res){
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;	
	
	Follow.find({userid:userid} ,function (err, followusers) {
		
		if (err)
		{
			resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
		}
		
		if(followusers.length == 0)
		{
			resmessage.genResponse(res,false,0,'',[],'Videos Returned Successfully.');
		}
		else
		{
			var userids = [];
			for(var i = 0 ; i < followusers.length ; i++)
			{
				userids.push(followusers[i].following_userid);
			}
			 
			Postfile.find({'$and' :[{status:"active"}, {"createdby" : { $in : userids}}, {'$or' :[{filepath: { $regex: '.*.mp4.*' } }, {filepath: { $regex: '.*.avi.*' } }, {filepath: { $regex: '.*.mkv.*' } }, {filepath: { $regex: '.*.mpeg.*' } } ]}]})
			.limit(numPerPage)
			.skip(numPerPage * page)
			.sort({'createdon': -1})
			.populate({path: 'postid', select: 'message location liked like_normal like_star like_heart'})
			.populate({path: 'createdby', select: 'firstname lastname username email mobileno profileimage follower following'})
			.exec(function (err, filesdata) {
								
				resmessage.genResponse(res,false,0,'',filesdata,'Following people videos successfully');
			});
		}
	});
};

exports.resharePost = function (req, res){
	var userid = req.userid;
	
	var postid = req.params.id;
	console.log("postid : "+ postid);
	if(postid == "" || postid == undefined || postid == null)
	{
		resmessage.genResponse(res,true,0,'',"",'Field missing(postid).');
		return;
	}
	
	Post.findById(postid, function (err, postdatas) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
			return;
		}
		
		Postfile.find({"postid" : postid , "status" : "active"} ,function (err, postfilesdata) {
			
			Postmessagetag.find({"postid" : postid} ,function (err, postmessagetagdata) {
				
				Posthash.find({"postid" : postid} ,function (err, posthashdata) {
				
					var postdata = new Post();
					postdata.message = postdatas.message;
					postdata.location = postdatas.location;
					postdata.posttype = postdatas.posttype;
					postdata.sharetype = 'share';
					postdata.filepath = postdatas.filepath;
					postdata.createdby = userid;
					postdata.createdbyy = userid;
					postdata.hostuser = postdatas.hostuser;
					
					postdata.save(function (err) {
						if(err)
						{
							resmessage.genResponse(res,true,0,'',err,'Connection error');
						}
						else
						{
							console.log("Insert Done With PostId : " + postdata._id);
							var newpostid = postdata._id;
							
							//Update Share Count on Old Post
							var postUpdate = new Post(postdatas);
							postUpdate.shared = parseInt(postdatas.shared) + 1;
							
							postUpdate.save(function (err) {
								if (err)
								{
									resmessage.genResponse(res,true,0,'','','Error. '+err);
									return;
								}
								
								console.log("Updated Post Share Count.");
								
								var Promise = require('bluebird');
							
								Promise.each(postfilesdata, function(item) {
									
									var newfiledata = new Postfile();
									newfiledata.postid = newpostid;
									newfiledata.filepath = item.filepath;
									newfiledata.createdby = userid;
									
									newfiledata.save(function (err) {
									});
								}).then(function(result) {
								    // This not run
								}).catch(function(rejection) {
								    
								});
								
								Promise.each(postmessagetagdata, function(item) {
									var tagdata = new Postmessagetag();
									tagdata.postid = newpostid;
									tagdata.taggeduserid = item.taggeduserid;
									tagdata.tagkey = item.tagkey;
									tagdata.createdby = userid;
									
									tagdata.save(function (err) {
									});
								}).then(function(result) {
								    // This not run
								}).catch(function(rejection) {
								    
								});
								
								Promise.each(posthashdata, function(itemhash) {
									var hashdata = new Posthash();
									hashdata.postid = newpostid;
									hashdata.hashvalue = itemhash.hashvalue;
									hashdata.createdby = userid;
									
									hashdata.save(function (err) {
									});	
								}).then(function(result) {
								    // This not run
								}).catch(function(rejection) {
								    
								});
								
								data = {
										"postId" : newpostid
									};
									
								resmessage.genResponse(res,false,0,'',data,'Post reshared successfully');
							});							
						}
					});
				});
			});
		});
	});
};

exports.getHashPostLists = function (req, res){
	var userid = req.userid;
	
	var numPerPage = parseInt(req.query.perpage, 10) || 10;
	var page = ((req.query.page == undefined || req.query.page == null) ? 1 : req.query.page);
	page = page-1;
	
	var jsonData = req.body;
	
	var userid = req.userid;
	var hashvaluee = ((jsonData.hashvalue == undefined || jsonData.hashvalue == null) ? "" : jsonData.hashvalue);
		
	Posthash.find({hashvalue:hashvaluee})
	.limit(numPerPage)
	.skip(numPerPage * page)
	.sort({'createdon': -1})
	//.populate({path: 'postid', select: 'message location liked like_normal like_star like_heart filepath'})
	.exec(function (err, posthashdatas) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
			return;
		}
		
		userids = [];
		postids = [];
		
		for(var i = 0 ; i < posthashdatas.length ; i++)
		{
			userids.push(posthashdatas[i].createdby);
			postids.push(posthashdatas[i].postid);
		}
		
		Post.find({"_id" : { $in : postids}})
		.populate({path: 'createdbyy', select: 'firstname lastname email profileimage username follower following'})
		.exec(function (err, postdatas) {
			if (err)
			{
				resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
				return;
			}
			
			Follow.find({'$and' :[{userid:userid}, {"following_userid" : { $in : userids}} ]} ,function (err, chkfolloweddata) {
											
				Postfile.find({"postid" : { $in : postids} , "status" : "active"} ,function (err, postfilesdata) {
					
					Postlike.aggregate([
					  {$match: {"postid" : { $in : postids}}},
					  {$sort: {created: -1}},
					  {$group: {_id:'$postid', title:{$push: '$createdby'}}},
					  {$project: {postid: '$_id', createdUserIds: '$title'}}] ,function (err, reslikesdata) {
					
						var likesdata = [];
						var likeduserids = [];
						//console.log("132 : "+JSON.stringify(reslikesdata));
						
						for(var i = 0 ; i < reslikesdata.length ; i++)
						{
							var tmpids = [];
							var createdUserIds = reslikesdata[i].createdUserIds;
							if(createdUserIds.length > 0)
							{
								tmpids.push(createdUserIds[0].toString());
								likeduserids.push(createdUserIds[0].toString())
							}
							if(createdUserIds.length > 1)
							{
								tmpids.push(createdUserIds[1].toString());
								likeduserids.push(createdUserIds[1].toString())
							}								
							
							var lcri = {"postid" : reslikesdata[i].postid, "userids" : tmpids, "userdetails" : []};
							likesdata.push(lcri);
						}
						
						likeduserids = likeduserids.filter((v, k, a) => a.indexOf(v) === k);
						
						User.find({"_id" : { $in : likeduserids}} ,function (err, likedusersdata) {
							var likesdata_new = [];
							
							for(var k = 0 ; k < likesdata.length ; k++)
							{
								var tmpUsrArr = [];
								var usrIds = likesdata[k].userids;
								
								for(var j = 0 ; j < usrIds.length ; j++)
								{
									for(var i = 0 ; i < likedusersdata.length ; i++)
									{
										if(usrIds[j] == likedusersdata[i]._id)
										{
											var cri = {};
											cri["userid"] = likedusersdata[i]._id;
											cri["firstname"] = likedusersdata[i].firstname;
											cri["lastname"] = likedusersdata[i].lastname;
											cri["username"] = likedusersdata[i].username;
											cri["profileimage"] = likedusersdata[i].profileimage;
											
											tmpUsrArr.push(cri);
											
											if(usrIds.length == tmpUsrArr.length)
											{
												break;
											}
										}
									}
								}
								
								likesdata_new.push({"postid" : likesdata[k].postid, "userids" : likesdata[k].userids, "userdetails" : tmpUsrArr});
							}
						
							Postlike.find({"postid" : { $in : postids} , "createdby" : userid} ,function (err, mypostlikesdata) {
								
								Postmessagetag.find({"postid" : { $in : postids}} ,function (err, mypostmessagetagdata) {
									
									Tag.find({"postid" : { $in : postids}})
									.populate({path: 'taggeduserid', select: 'firstname lastname email profileimage username'})
									.exec(function (err, postfiletagdata) {
									
										Posthash.find({"postid" : { $in : postids}} ,function (err, posthashdata) {
											var postarr = [];
										
											for(var ii = 0 ; ii < posthashdatas.length ; ii++)
											{
												for(var i = 0 ; i < postdatas.length ; i++)
												{
													if(posthashdatas[ii].postid.toString() == postdatas[i]._id)
													{
														var is_followed = "no";
														for(var j = 0 ; j < chkfolloweddata.length ; j++)
														{
															if(chkfolloweddata[j].following_userid == postdatas[i].createdbyy._id)
															{
																is_followed = "yes";
																break;
															}
														}
														
														var cri = {};
														cri["postid"] = postdatas[i]._id;
														cri["location"] = postdatas[i].location;
														cri["message"] = postdatas[i].message;
														cri["filepath"] = postdatas[i].filepath;
														cri["files"] = postdatas[i].files;
														cri["file_details"] = [];
														cri["liked"] = postdatas[i].liked;
														cri["like_normal"] = postdatas[i].like_normal;
														cri["like_star"] = postdatas[i].like_star;
														cri["like_heart"] = postdatas[i].like_heart;
														cri["favourite"] = postdatas[i].favourite;
														cri["starred"] = postdatas[i].starred;
														cri["shared"] = postdatas[i].shared;
														cri["userdetail"] = {
																"userid":postdatas[i].createdbyy._id,
																"firstname":postdatas[i].createdbyy.firstname,
																"lastname":postdatas[i].createdbyy.lastname,
																"email":postdatas[i].createdbyy.email,
																"profileimage":postdatas[i].createdbyy.profileimage,
																"username":postdatas[i].createdbyy.username,
																"follower":postdatas[i].createdbyy.follower,
																"following":postdatas[i].createdbyy.following,
																"is_followed":is_followed
															};
														cri["tagged_msg_users"] = [];
														cri["hash_list"] = [];
														
														for(var j = 0 ; j < postfilesdata.length ; j++)
														{
															if(cri["postid"] == (postfilesdata[j].postid).toString())
															{
																var postfileid = postfilesdata[j]._id;
																
																var tagarr = [];
																for(var jj = 0 ; jj < postfiletagdata.length ; jj++)
																{
																	if(postfileid.toString() == postfiletagdata[jj].postfileid.toString())
																	{
																		tagarr.push(postfiletagdata[jj]);
																	}
																}
																
																var filearra = {"_id" : postfilesdata[j]._id, "filepath" : postfilesdata[j].filepath, "createdon" : postfilesdata[j].createdon, "tags" : tagarr};
																cri["file_details"].push(filearra);
															}
														}
														
														for(var j = 0 ; j < mypostmessagetagdata.length ; j++)
														{
															if(cri["postid"] == (mypostmessagetagdata[j].postid).toString())
															{
																cri["tagged_msg_users"].push(mypostmessagetagdata[j]);
															}
														}
														
														for(var j = 0 ; j < posthashdata.length ; j++)
														{
															if(cri["postid"] == (posthashdata[j].postid).toString())
															{
																cri["hash_list"].push(posthashdata[j]);
															}
														}
														
														cri["liked_users"] = [];
														for(var j = 0 ; j < likesdata_new.length ; j++)
														{
															if(cri["postid"] == (likesdata_new[j].postid).toString())
															{
																cri["liked_users"] = likesdata_new[j].userdetails;
																break;
															}
														}
														
														cri["is_liked"] = 'no';
														cri["liketype"] = '';
														
														for(var j = 0 ; j < mypostlikesdata.length ; j++)
														{
															if(cri["postid"] == (mypostlikesdata[j].postid).toString())
															{
																cri["is_liked"] = 'yes';
																cri["liketype"] = mypostlikesdata[j].liketype;
																break;
															}
														}
														
														//postarr.push(cri);
														postarr.push({"_id" : posthashdatas[ii]._id, "hashvalue" : posthashdatas[ii].hashvalue, "createdon" : posthashdatas[ii].createdon, "createdon" : posthashdatas[ii].createdon, "postdetails" : cri});
														
														break;
													}												
												}											
											}										
											
											resmessage.genResponse(res,false,0,'',postarr,'Post hash lists returned successfully');
										});													
									});												
								});												
							});											
						});
					});
				});
			});
		});		
	});
};

exports.pushcreateby = function (req, res){
	Post.find()
	.exec(function (err, postdatas) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'','','Connection Error While Getting Details');
		}
		
		if(postdatas.length == 0)
		{
			resmessage.genResponse(res,false,0,'',[],'Posts returned successfully');
		}
		else
		{
			for(var i = 0 ; i < postdatas.length ; i++)
			{
				Post.findById(postdatas[i]._id, function (err, postdata) {
					var postUpdate = new Post(postdata);
					postUpdate.hostuser = postdata.createdby;
					
					postUpdate.save(function (err) {
						
					});
				});
			}				
		}
	});
};

function updateLikeCountFun(postid){
	
	Postlike.count({postid:postid, liketype : 'normal'} ,function (err, normallikes) {
		
		if (err)
		{
			console.log("Like Update Err 1 : "+err);
			return "";
		}
		
		Postlike.count({postid:postid, liketype : 'star'} ,function (err, starlikes) {
		
			if (err)
			{
				console.log("Like Update Err 2 : "+err);
				return "";
			}
			
			Postlike.count({postid:postid, liketype : 'heart'} ,function (err, heartlikes) {
		
				if (err)
				{
					console.log("Like Update Err 3 : "+err);
					return "";
				}
				
				Post.findById(postid, function (err, updatepost) {
					
					var postUpdate = new Post(updatepost);
					postUpdate.like_normal = normallikes;
					postUpdate.like_star = starlikes;
					postUpdate.like_heart = heartlikes;
					postUpdate.liked = parseInt(normallikes + starlikes + heartlikes);
					
					try
					{
						postUpdate.save(function (err) {
							if (err)
							{
								console.log("Like Update Err 4 : "+err);
								return "";
							}
							
							console.log("Updated Post Like Counts..");							
						});
					}
					catch (err)
					{
						console.log("TRY CATCH ERROR : "+err);
					}					
				});
			});	
		});
	});
}