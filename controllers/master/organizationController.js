var config = require('../../config/config');

const model = require('../../models/masterModel');
const resmessage = require('../../config/response');
const table = require('../../config/tableConstant');

exports.organizationList = function (req, res){
  let parameters = {
      tablename: table.TABLE_ORGANIZATION,
      where: 'status = 1',
  };
	model.listModel(parameters).then(result => {
    resmessage.genResponse(res,false,0,'organization',result.rows);
    },err=>{
      resmessage.genResponse(res,true,0,'organization',err);
	});	
};

exports.organizationSingle = function (req, res){
  console.log(req.params);
  let parameters = {
      tablename: table.TABLE_ORGANIZATION,
      where: 'status = 1 and organization_id='+req.params.id,
  };
	model.listModel(parameters).then(result => {
    resmessage.genResponse(res,false,0,'organization',result.rows);
    },err=>{
      resmessage.genResponse(res,true,0,'organization',err);
	});		
};

exports.createOrganization = function (req, res){
  let parameters = {
    tablename: table.TABLE_ORGANIZATION,
    where: "status=1 and organization_name='"+req.body.organization_name+"'",
  };
  let parameters1 = {
    tablename: table.TABLE_ORGANIZATION,
    fields: req.body,
    return: 'organization_id',
  };
  parameters1.fields.status = 1;
  parameters1.fields.created_on = config.currenttime;
  parameters1.fields.unique_hash = Date.now();
  model.listModel(parameters).then(result => {
    if(result.rows.length > 0){
      resmessage.genResponse(res,true,4,'organization name');
    }
    else{
      model.createModel(parameters1).then(result => {
        resmessage.genResponse(res,false,1,'organization',result.rows);
      },err=>{
        resmessage.genResponse(res,true,1,'organization',err);
      });	
    }
  },err=>{
    resmessage.genResponse(res,true,1,'organization',err);
  })
};

exports.updateOrganization = function (req, res){
  let parameters = {
    tablename: table.TABLE_ORGANIZATION,
    where: "status=1 and organization_id != "+req.params.id+" and organization_name='"+req.body.organization_name+"'",
  };
  let parameters1 = {
    tablename: table.TABLE_ORGANIZATION,
    fields: req.body,
    where: 'organization_id='+req.params.id,
    return: 'organization_id',
  };
  parameters1.fields.updated_on = config.currenttime;
  model.listModel(parameters).then(result => {
    if(result.rows.length > 0){
        resmessage.genResponse(res,true,4,'organization name');
    }
    else{
      model.updateModel(parameters1).then(result => {
            resmessage.genResponse(res,false,2,'organization',result.rows);
        },err=>{
            resmessage.genResponse(res,true,2,'organization',err);
      });		
    }
  },err=>{
    resmessage.genResponse(res,true,2,'organization',err);
  });
};

exports.deleteOrganization = function (req, res){
	let parameters = {
    tablename: table.TABLE_ORGANIZATION,
    fields: {
      status: 3,
      updated_on: config.currenttime
    },
    where: 'organization_id='+req.params.id,
    return: 'organization_id',
  };
	model.updateModel(parameters).then(result => {
    resmessage.genResponse(res,false,3,'organization',result.rows);
    },err=>{
      resmessage.genResponse(res,true,3,'organization',err);
	});	
};