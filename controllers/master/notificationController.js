const config = require('../../config/config');
const resmessage = require('../../config/response');
User = require('../../models/userModel');
Notification = require('../../models/notificationModel');

exports.updateUserFCMToken = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var tokenuserid = ((jsonData.userid == undefined || jsonData.userid == null) ? "" : jsonData.userid);
	var fcmtoken = ((jsonData.fcmtoken == undefined || jsonData.fcmtoken == null) ? "" : jsonData.fcmtoken);
	
	User.findById(tokenuserid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			var userUpdate = new User(updateuser);
			userUpdate.fcmtoken = fcmtoken;
			
			userUpdate.save(function (err) {
				if (err)
				{
					resmessage.genResponse(res,true,0,'',"",'Connection Error While Updating Details');
				}
				
				console.log("Updated User Profile.");
				
				resmessage.genResponse(res,false,0,'','','User FCM Token updated successfully');
			});
		}
	});
};

exports.testFCMMessage = function (req, res){
	var jsonData = req.body;
	
	var userid = req.userid;
	var registration_token = ((jsonData.registration_token == undefined || jsonData.registration_token == null) ? "" : jsonData.registration_token);
	var collapse_key = ((jsonData.collapse_key == undefined || jsonData.collapse_key == null) ? "" : jsonData.collapse_key);
	var notititle = ((jsonData.notititle == undefined || jsonData.notititle == null) ? "" : jsonData.notititle);
	var notibody = ((jsonData.notibody == undefined || jsonData.notibody == null) ? "" : jsonData.notibody);
	var notidata = ((jsonData.notidata == undefined || jsonData.notidata == null) ? {} : jsonData.notidata);
	
	var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: registration_token, 
        //collapse_key: collapse_key,
        
        notification: {
            title: notititle, 
            body: notibody 
        },        
        /*data: {  //you can send only notification or only data(or include both)
            my_key: 'my value',
            my_another_key: 'my another value'
        }*/
        data: notidata
    };
	
	if(collapse_key != '')
	{
		var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: registration_token, 
        collapse_key: collapse_key,
        
        notification: {
            title: notititle, 
            body: notibody 
        },        
        /*data: {  //you can send only notification or only data(or include both)
            my_key: 'my value',
            my_another_key: 'my another value'
        }*/
        data: notidata
    };
	}
	
	var FCM = require('fcm-node');
	var fcm = new FCM(process.env.FCM_ACCESS_KEY);
	
	fcm.send(message, function(err, response){
        if (err) {
			resmessage.genResponse(res,true,0,'',"",'Error in FCM : '+err);
            console.log("Something has gone wrong!");
        } else {
			resmessage.genResponse(res,false,0,'',response,'User FCM message sent successfully');
        }
    });
};

exports.getMyNotificationList = function (req, res){
	var userid = req.userid;
		
	User.findById(userid, function (err, updateuser) {
		if (err)
		{
			resmessage.genResponse(res,true,0,'',"",'Connection Error While Getting Details');
		}
		if(updateuser == null)
		{
			resmessage.genResponse(res,true,0,'',"",'Invalid token passed.');
		}
		else
		{
			//Notification.find({notifyuserid : userid})
			Notification.find({'$and' :[{notifyuserid:userid}, {createdby:{ $ne: userid }} ]})
			.populate({path: 'createdby', select: 'firstname lastname email profileimage username'})
			.populate({path: 'postid', select: 'message location posttype filepath liked'})
			.sort({'createdon': -1})
			.exec(function (err, postdatas) {
				if (err)
				{
					console.log(err);
					resmessage.genResponse(res,true,0,'',err,'Connection Error While Getting Details');
					return;
				}
				
				resmessage.genResponse(res,false,0,'',postdatas,'Notifications returned successfully');
			});
		}
	});
};

exports.triggerNotification = function(notificationid){
	console.log("Notification trggering started.");
	
	Notification.findById(notificationid)
	.populate({path: 'notifyuserid', select: 'firstname lastname email profileimage username fcmtoken'})
	.populate({path: 'createdby', select: 'firstname lastname email profileimage username fcmtoken'})
	.populate({path: 'postid', select: 'message location posttype filepath liked'})
	.exec(function (err, notifydatas) {
		if (err)
		{
			console.log("Notify Update Err 1 : "+err);
			return "";
		}
		
		if(notifydatas.notifyuserid.fcmtoken != '')
		{
			console.log("FCM Token : "+notifydatas.notifyuserid.fcmtoken);
			try
			{
				var notititle = '';
				var notibody = '';
				var notidata = '';
				
				if(notifydatas.notificationtype == 'post-like')
				{
					notititle = notifydatas.createdby.firstname+" liked your post.";
					notibody = notifydatas.createdby.firstname+" liked your post.";
					notidata = notifydatas.postid;
				}
				else if(notifydatas.notificationtype == 'post-unlike')
				{
					notititle = notifydatas.createdby.firstname+" unliked your post.";
					notibody = notifydatas.createdby.firstname+" unliked your post.";
					notidata = notifydatas.postid;
				}
				else if(notifydatas.notificationtype == 'follow')
				{
					notititle = notifydatas.createdby.firstname+" followed you.";
					notibody = notifydatas.createdby.firstname+" followed you.";
					notidata = notifydatas.createdby;
				}
				
				var message = {
			        to: notifydatas.notifyuserid.fcmtoken, 
			        notification: {
			            title: notititle, 
			            body: notibody 
			        },
			        data: notidata
			    };
				
				var FCM = require('fcm-node');
				var fcm = new FCM(process.env.FCM_ACCESS_KEY);
				
				fcm.send(message, function(err, response){
			        if (err) {
						console.log("FCM Notification Err : "+err);
			        } else {
						console.log("FCM Notification Response : "+response);
			        }
			    });
			}
			catch (err)
			{
				console.log("TRY CATCH ERROR : "+err);
			}
		}
		else
		{
			console.log("No FCM Token");
		}
	});
}