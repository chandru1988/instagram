require('dotenv').config();
var mysql = require('promise-mysql');

class database{

	connectDB() {
		return mysql.createConnection({
			host: process.env.DB_HOST,
			user: process.env.DB_USER,
			password: process.env.DB_PASSWORD,
			database: process.env.DB_DATABASE
		});
	}

	async query(sql){
		return new Promise(async (resolve, reject) => {
			let connection = await this.connectDB();
			try {
				var peakData = await connection.query(sql);
				resolve(peakData);
			} catch(err) {
				await connection.end();
				reject(err);
			}
		});
	}
}

const db = new database();
module.exports = db;