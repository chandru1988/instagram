let sucmessage = [
	'Data returned successfully',
	'%name% created successfully',
	'%name% updated successfully',
	'%name% deleted successfully',
];
let errmessage = [
	'There is some problem in list %name%',
	'There is some problem in create %name%',
	'There is some problem in update %name%',
	'There is some problem in delete %name%',
	'Already this %name% is used',
];

exports.genResponse = function(res,iserror,type,name='',data='',message=''){
    let response = {};
    response.iserror = iserror;
	
    if(!iserror){
        response.message = sucmessage[type].replace("%name%", name.capitalize());
        if(Array.isArray(data)){
            response.data = data
        }
		else if(data == ''){
            /*response.iserror = true;
            response.message = 'There is no data to return';*/
            if(data != ''){ response.data = data }
        }
        else{
            if(data != ''){ response.data = data }
        }
    }
    else{
        response.message = errmessage[type].replace("%name%", name);
        if(data != ''){ response.data = data }
    }
    if(message){
        response.message = message;
    }
	
	if (response.iserror) {
		res.status(200).json(response);
	} else {
		res.status(200).json(response);
	}
};

exports.genProcedureResponse = function(res,iserror,message=''){
    let response = {};
    response.iserror = iserror;
    response.message = message;
    if (response.iserror) {
		res.status(500).json(response);
	} else {
		res.status(200).json(response);
	}
}

String.prototype.capitalize = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
};
