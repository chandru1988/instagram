require('dotenv').config();
var mysql = require('promise-mysql');

class database_read{

	connectDBRead() {
		return mysql.createConnection({
			host: process.env.DB_READ_HOST,
			user: process.env.DB_READ_USER,
			password: process.env.DB_READ_PASSWORD,
			database: process.env.DB_READ_DATABASE
		});
	}

	async query(sql){
		return new Promise(async (resolve, reject) => {
			let connection_read = await this.connectDBRead();
			try {
				var peakData = await connection_read.query(sql);
				resolve(peakData);
			} catch(err) {
				await connection_read.end();
				reject(err);
			}
		});
	}
}

const db_read = new database_read();
module.exports = db_read;