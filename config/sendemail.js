var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var handlebars = require('handlebars');
var fs = require('fs');

var config = require('./config');

class sendemail{
	async sendmail(params,cb){
		
		var readHTMLFile = function(path, callback) {
			fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
				if (err) {
					callback(err);
				}
				else {
					callback(null,html);
				}
			});
		};
		
		smtpTransport = nodemailer.createTransport({
			host: "smtp.gmail.com",
			port: 587,
			secure: false,
			auth: {
			  user: 'ashokkumar.k@vivensas.com',
			  pass: 'ashokvivensas'
			}
		});
		
		readHTMLFile(__dirname + '/../emailtemplate/emailtemplate.html', function(err, html) {
			if(err)
				cb(null,err);
			
			var template = handlebars.compile(html);
			var replacements = {
				themeurl: config.themeurl,
				subject: params.subject,
				content: params.content,
				heading: params.heading,
				facebooklink: config.facebooklink,
				twitterlink: config.twitterlink,
			};
			var htmlToSend = template(replacements);
			var mailOptions = {
				from: config.emailfromname+' <'+config.emailfromemail+'>',
				to : params.toemail,
				subject : params.subject,
				html : htmlToSend
			};
			/*smtpTransport.sendMail(mailOptions, function (error, response) {
				if (error) {
					cb(null,error);
				}
				else{
					cb(response)
				}
			});*/
			cb("success")
		});

	}



}

module.exports = new sendemail();