var dateTime = require('node-datetime');

module.exports = {
	keylabel: 'auth-key',
	saltrounds: 10,
	tokensecret: 'instagram',
	toekenexpiry: '10y',
	currenttime: dateTime.create().format('Y-m-d H:M:S'),
	emailfromname: 'Instagram',
	emailfromemail: 'ashokkumar.k@vivensas.com',
	themeurl: 'http://35.222.92.205:3000/',
	facebooklink: 'http://facebook.com',
	googlelink: 'http://google.com',
	emailheader:'<div style="width: 100%; background-color: #f9f9f9; margin: 0px; padding: 10px;"><table style="width: 100%; max-width: 600px; margin: 0px auto; font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; background-color: #ffffff; border-collapse: collapse;"><tr><td colspan="2" style="text-align: center; padding: 10px; border-bottom: 2px solid #14b481;"><a href="javascript:void(0);" target="_blank"><img src="'+process.env.PROJECT_BASEURL+'uploads/logo.jpg" alt="Famewal" style="width: 30%;" /></a></td></tr>',
	emailfooter:'<tr><td style="padding: 10px; font-size: 12px;"><div>Famewal</div></td><td style="padding: 10px; font-size: 12px;"><div>Email: contact@famewal.com</div></td></tr><tr><td colspan="2" style="padding: 10px; font-size: 10px; text-align: center; color: #9b9b9b;">&copy;2022-Famewal. All rights reserved.</td></tr></table><div style="text-align: center; font-size: 10px; padding: 10px; font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;"><!--<a href="#" style="color: #15c;">Unsubscribe here</a>--></div></div>'
};
