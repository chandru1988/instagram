const express = require('express');
const bodyparser = require('body-parser');
const expressValidator = require('express-validator');
const http = require('http');
const path = require('path');

const {dbconnection} = require('./lib/connections');

const authroutes = require('./routes/authRoutes');
const postroutes = require('./routes/postRoutes');
const followroutes = require('./routes/followRoutes');
const storyroutes = require('./routes/storyRoutes');
const tagroutes = require('./routes/tagRoutes');
const emailroutes = require('./routes/emailRoutes');
const notificationroutes = require('./routes/notificationRoutes');

class Server {
	constructor() {
		this.port = process.env.PORT || 3000;
		// this.host = 'localhost';

		this.app = express();
		this.http = http.Server(this.app);
	}

	appConfig() {
		this.app.use(function (req, res, next) {
			res.setHeader('Access-Control-Allow-Origin', '*');
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
			res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,auth-key'); // If needed
			res.setHeader('Access-Control-Allow-Credentials', true); // If needed
			next();
		});
		this.app.use(bodyparser.urlencoded({ extended: true }));
		this.app.use(bodyparser.json());
		// this.app.use(expressValidator);
		this.app.use('/', express.static(path.join(__dirname, 'public')));
		this.app.use('/assets',express.static(path.join(__dirname, 'assets')));
		this.app.use('/uploads',express.static(path.join(__dirname, 'uploads')));
	}

	includeRoutes() {
		this.app.use('/', authroutes);
		this.app.use('/', postroutes);
		this.app.use('/', followroutes);
		this.app.use('/', storyroutes);
		this.app.use('/', tagroutes);
		this.app.use('/', emailroutes);
		this.app.use('/', notificationroutes);
	}

	initial() {
		this.appConfig();
		this.includeRoutes();
		this.app.get('*', (req, res) => res.status(404).json({ url: 'Request '+req.originalUrl + ' not found' }) );
		this.app.post('*', (req, res) => res.status(404).json({ url: 'Request '+req.originalUrl + ' not found' }) );
		this.app.put('*', (req, res) => res.status(404).json({ url: 'Request '+req.originalUrl + ' not found' }) );
		this.app.delete('*', (req, res) => res.status(404).json({ url: 'Request '+req.originalUrl + ' not found' }) );
		
		this.http.listen(this.port, () => {
			console.log(`Server running at port:${this.port}`);
		});
	}
}

const app = new Server();
app.initial();
