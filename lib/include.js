
mysql = require("mysql");
mongoose = require("mongoose");
config = require("config");
fs = require("fs");
commonJs = require("./commonValidation");
process.env.NODE_ENV = "local";

console.log("NODE_ENV: " + config.util.getEnv("NODE_ENV"));
console.log("NODE_CONFIG_DIR: " + config.util.getEnv("NODE_CONFIG_DIR"));
console.log("NODE_CONFIG: " + config.util.getEnv("NODE_CONFIG"));
//console.log("mysql_host: " + config.get("mysql.host"));

// validation
Joi = require("@hapi/joi");
joiOption = {
    abortEarly: false,
    noDefaults: true,
    stripUnknown: true,
    allowUnknown: true,
    language: {
        key: "{{key}} "
    }
};
//Date and Time
 //moment = require('moment');
 //moment().format('yyyy-mm-dd:hh:mm:ss');
