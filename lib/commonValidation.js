var globalfunctions = (function () {
    var _parameterValidation = function (value) {
        if (
            typeof value !== "undefined" &&
            value !== null &&
            value !== "null" &&
            value !== "" &&
            value != "undefined"
        ) {
            value = value.toString();
            value = value.trim();
            if (
                value == "" ||
                value == null ||
                value == "null" ||
                typeof value === "undefined"
            ) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    };

    var _usercontain = function (firebase_id, arr) {
        var found = false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == firebase_id) {
                found = true;
                break;
            }
        }
        return found;
    };

    var _isValidEmail = function (email) {
        try {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        } catch (e) {
            return false;
        }
    };
    
    var _decryptUserTokenValue = function (userToken) {
        try {
        	if(userToken == undefined)
        	{
				resultObtained = {
		            result: 'UserToken Missing',
		            code: 404,
		            data: []
		        };
		        
		        return resultObtained;
			}
			
            let buff = new Buffer(userToken, 'base64');
			let text = buff.toString('ascii');
			
			var resultObtained = {};
			var userDetails = JSON.parse(text);
			if(userDetails.userid == undefined)
			{
				resultObtained = {
		            result: 'Invalid UserToken',
		            code: 401,
		            data: []
		        };
		        
		        return resultObtained;
			}
			
			var valid_till = new Date(userDetails.valid_till);
			var curdate = new Date();
			if (valid_till.getTime() < curdate.getTime()) 
			{
				resultObtained = {
		            result: 'UserToken Expired',
		            code: 402,
		            data: []
		        };
		        
		        return resultObtained;
			}
			
            resultObtained = {
		            result: "Done",
		            code: 200,
		            data: userDetails
		    };
		    
		    return resultObtained;
        } catch (e) {
            resultObtained = {
		            result: "Invalid UserToken.",
		            code: 403,
		            data: []
		        };
		    return resultObtained;
        }
    };

    var _isValidJson = function (str) {
        try {
            var obj = JSON.parse(str);
            return !!obj && typeof obj === "object";
        } catch (e) {
            return false;
        }
    };

    var _isValidArray = function (str) {
        try {
            return Array.isArray(str);
        } catch (e) {
            return false;
        }
    };

    var _getParamValidationError = function (error) {
        var customizeError = [];
        var errorObtained = error.details;
        if (errorObtained != "undefined") {
            errorObtained.forEach(function (value, index) {
                customizeError.push({
                    error_message: value.message,
                    path: value.path
                });
            });
        }
        return customizeError;
    };
    var _getErrorMessage = function (error) {
        if (_parameterValidation(error)) {
            var key = "";
            for (var key in error.errors) {
                key = key;
                break;
            }
            if (key != "") {
                var custome_errors = [];
                var i = 0;
                for (var key in error.errors) {
                    key_name = error.errors[key].message;
                    key_err_name = key.replace("unique.", "");
                    if (key_name.toLowerCase().indexOf("cast to number failed") >= 0) {
                        custome_errors[i] = key_name.replace(
                            "Cast to Number failed",
                            "Not a Number"
                        );
                    } else {
                        keyvalue = error.errors[key].value;
                        var customeErrorsArr = {};
                        customeErrorsArr.fieldName = key_err_name;
                        customeErrorsArr.errMsg = key_name;
                        customeErrorsArr.error_message = key_name.replace(key, keyvalue);
                        custome_errors[i] = customeErrorsArr;
                    }
                    i++;
                }
                return custome_errors;
            } else if (_parameterValidation(error.message)) {
                if (typeof error.message.toLowerCase() !== "undefined") {
                    if (
                        error.message.toLowerCase().indexOf("cast to number failed") >= 0
                    ) {
                        return error.message.replace(
                            "Cast to Number failed",
                            "Not a Number"
                        );
                    } else {
                        return error.message;
                    }
                } else {
                    return error.message;
                }
            } else if (error.errors) {
                return error.errors;
            } else {
                return "";
            }
        } else if (error == null) {
            return error_no_result;
        } else {
            return "Error Occurred!";
        }
    };

    var _isEmpty = function isEmpty(obj) {
        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length === 0) return true;
        if (obj.length > 0) return false;

        // If it isn't an object at this point
        // it is empty, but it can't be anything *but* empty
        // Is it empty?  Depends on your application.
        if (typeof obj !== "object") return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    };
    return {
        parameterValidation: _parameterValidation,
        isValidEmail: _isValidEmail,
        isValidJson: _isValidJson,
        decryptUserTokenValue: _decryptUserTokenValue,
        isValidArray: _isValidArray,
        getParamValidationError: _getParamValidationError,
        getErrorMessage: _getErrorMessage,
        isEmpty: _isEmpty,
        usercontain: _usercontain
    };
})();

module.exports = globalfunctions;
