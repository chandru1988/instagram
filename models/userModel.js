var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');

// Setup schema
var userSchema = mongoose.Schema({
					firstname: {
						type: String,
						default: ""
					},
					lastname: {
						type: String,
						default: ""
					},
					email: {
						type: String,
						default: ""
					},
					password: {
						type: String,
						default: ""
					},
					temp_password: {
						type: String,
						default: ""
					},
					salt: {
						type: String,
						default: ""
					},
					emailOTP: {
						type: String,
						default: ""
					},
					mobileno: {
						type: String,
						default: ""
					},
					profileimage: {
						type: String,
						default: ""
					},
					oauthid: {
						type: String,
						default: ""
					},
					website: {
						type: String,
						default: ""
					},
					bio: {
						type: String,
						default: ""
					},
					gender: {
						type: String,
						default: ""
					},
					username: {
						type: String,
						default: ""
					},
					location: {
						type: String,
						default: ""
					},
					status: {
						type: String,
						default: "notverified"
					},
					follower: {
						type: Number,
						default: "0"
					},
					following: {
						type: Number,
						default: "0"
					},
					fcmtoken: {
						type: String,
						default: ""
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					lastloggedin: {
						type: Date,
						default: ""
					}
				});

// Export User model
var User = module.exports = dbconnection.model('user', userSchema);

module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}