var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const Post = require("./postModel");

// Setup schema
var storySchema = mongoose.Schema({
					message: {
						type: String,
						default: ""
					},
					filetype: {
						type: String,
						default: ""
					},
					filepath: {
						type: String,
						default: ""
					},
					seen: {
						type: Number,
						default: "0"
					},
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					status: {
						type: String,
						default: "active"
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:'User'
					}
				});

// Export Story model
var Story = module.exports = dbconnection.model('story', storySchema);

module.exports.get = function (callback, limit) {
    Story.find(callback).limit(limit);
}