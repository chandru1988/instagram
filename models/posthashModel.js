var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");
const Post = require("./postModel");

// Setup schema
var posthashSchema = mongoose.Schema({
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					hashvalue: {
						type: String,
						default: ""
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export User model
var Posthash = module.exports = dbconnection.model('posthash', posthashSchema);

module.exports.get = function (callback, limit) {
    Posthash.find(callback).limit(limit);
}