var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");
const Post = require("./postModel");

// Setup schema
var notificationSchema = mongoose.Schema({
					notificationtype: {
						type: String,
						default: "post-like"
					},
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					notifyuserid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export Notification model
var Notification = module.exports = dbconnection.model('notification', notificationSchema);

module.exports.get = function (callback, limit) {
    Notification.find(callback).limit(limit);
}