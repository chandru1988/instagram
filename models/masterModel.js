const DB = require('../config/db');
const DB_READ = require('../config/db_read');

class webModel{

	constructor(app) {
		this.db = DB;
		this.db_read = DB_READ;
	}

	customQuery(sql) {
		return new Promise((resolve, reject) => {
			this.db.query(sql).then(res => resolve(res), err => reject(err)).catch(err => reject(err));;
		});
	}

	listModel(param) {
		return new Promise((resolve, reject) => {
            let sql = 'select * from '+param.tablename;
            if(param.where){
                sql += ' where '+param.where;
            }
            if(param.orderby){
                sql += ' '+param.orderby;
            }
            if(param.limit){
                sql += ' limit '+param.limit;
            }
            console.log(sql);
			this.db_read.query(sql).then(res => resolve(res), err => reject(err)).catch(err => reject(err));;
		});
	}

	listModelFromMain(param) {
		return new Promise((resolve, reject) => {
            let sql = 'select * from '+param.tablename;
            if(param.where){
                sql += ' where '+param.where;
            }
            if(param.orderby){
                sql += ' '+param.orderby;
            }
            if(param.limit){
                sql += ' limit '+param.limit;
            }
            console.log(sql);
			this.db.query(sql).then(res => resolve(res), err => reject(err)).catch(err => reject(err));;
		});
	}

	createModel(param) {
        let insertkey = [];
        let insertvalue = [];
        if(param.fields){
            for (let [key, value] of Object.entries(param.fields)) {
                insertkey.push(key);
                if(value != ''){
                    insertvalue.push("'"+value+"'");
                }
                else{
                    insertvalue.push("null");
                }
            }
            return new Promise((resolve, reject) => {
                let sql = 'insert into '+param.tablename+' ('+insertkey.join(',')+') values ('+insertvalue.join(',')+')';
                if(param.return){
                    //sql += ' RETURNING '+param.return;
                } 
                console.log(sql);
                this.db.query(sql).then(res => resolve(res.insertId), err => reject(err)).catch(err => reject(err));
            });
        }
	}

	updateModel(param) {
		let updatevalue = [];
        if(param.fields){
            for (let [key, value] of Object.entries(param.fields)) {
                if(value != ''){
                    updatevalue.push(key+"='"+value+"'");
                }
                else{
                    updatevalue.push(key+"= null");
                }
            }
            return new Promise((resolve, reject) => {
                let sql = 'update '+param.tablename+' set '+updatevalue;
                if(param.revision){
                    sql += ' ,revision = revision + 1 ';
                }
                if(param.where){
                    sql += ' where '+param.where;
                }
                if(param.return){
                    //sql += ' RETURNING '+param.return;
                }
                console.log(sql);
                this.db.query(sql).then(res => resolve(res), err => reject(err)).catch(err => reject(err));
            });
        }
	}

	procedureModel(params) {
		return new Promise((resolve, reject) => {
            this.db.procedure(params).then(res=>{
                resolve(res)
            },err=>{
                reject(err);
            });
		});
	}

	// deleteModel(param) {
	// 	return new Promise((resolve, reject) => {
	// 		let sql = 'update '+param.tablename+' set '+param.updatedata;
    //         if(param.where){
    //             sql += ' where '+param.where;
    //         }
    //         if(param.return){
    //             sql += ' RETURNING '+param.return;;
    //         }
    //         console.log(sql);
	// 		this.db.query(sql).then(res => resolve(res), err => reject(err));
	// 	});
	// }



}

module.exports = new webModel();