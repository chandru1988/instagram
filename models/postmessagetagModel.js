var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");
const Post = require("./postModel");

// Setup schema
var postmessagetagSchema = mongoose.Schema({
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					taggeduserid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					},
					tagkey: {
						type: String,
						default: ""
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export User model
var Postmessagetag = module.exports = dbconnection.model('postmessagetag', postmessagetagSchema);

module.exports.get = function (callback, limit) {
    Postmessagetag.find(callback).limit(limit);
}