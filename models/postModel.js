var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");

// Setup schema
var postSchema = mongoose.Schema({
					message: {
						type: String,
						default: ""
					},
					location: {
						type: String,
						default: ""
					},
					posttype: {
						type: String,
						default: "followers" //followers/public
					},
					sharetype: {
						type: String,
						default: "new" //new/share
					},
					filepath: {
						type: String,
						default: ""
					},
					files: {
						type: {},
						default: []
					},
					liked: {
						type: Number,
						default: "0"
					},
					like_normal: {
						type: Number,
						default: "0"
					},
					like_star: {
						type: Number,
						default: "0"
					},
					like_heart: {
						type: Number,
						default: "0"
					},
					favourite: {
						type: Number,
						default: "0"
					},
					starred: {
						type: Number,
						default: "0"
					},
					shared: {
						type: Number,
						default: "0"
					},
					status: {
						type: String,
						default: "active"
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: String,
						default: ""
					},
					createdbyy: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					},
					hostuser: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export User model
var Post = module.exports = dbconnection.model('post', postSchema);

module.exports.get = function (callback, limit) {
    Post.find(callback).limit(limit);
}