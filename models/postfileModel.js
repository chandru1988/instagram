var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");
const Post = require("./postModel");

// Setup schema
var postfileSchema = mongoose.Schema({
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					filepath: {
						type: String,
						default: ""
					},
					liked: {
						type: Number,
						default: "0"
					},
					favourite: {
						type: Number,
						default: "0"
					},
					starred: {
						type: Number,
						default: "0"
					},
					status: {
						type: String,
						default: "active"
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export Postfile model
var Postfile = module.exports = dbconnection.model('postfile', postfileSchema);

module.exports.get = function (callback, limit) {
    Postfile.find(callback).limit(limit);
}