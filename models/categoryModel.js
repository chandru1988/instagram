const DB = require('../config/db');
const table = require('../config/tableConstant');

class webModel{

	constructor(app) {
		this.db = DB;
	}

	getProductCategoriesByParentId(param) {
		return new Promise((resolve, reject) => {
			let sql = '';
			if(param.id){
				sql += `WITH RECURSIVE category_path (id, title, parent_id) AS
						(
						SELECT product_category_id, product_category_name, parent_category
							FROM ${table.TABLE_PRODUCT_CATEGORY}
							WHERE product_category_id = ${param.id} and status = 1
						UNION ALL
						SELECT c.product_category_id, c.product_category_name, c.parent_category
							FROM category_path AS cp JOIN ${table.TABLE_PRODUCT_CATEGORY} AS c
							ON cp.id = c.parent_category where c.status = 1
						)
						SELECT * FROM category_path`;
			}
			else{
				sql += `WITH RECURSIVE supplytree AS
						(SELECT product_category_id, product_category_name, parent_category, CAST(product_category_name As varchar(100)) As category_name
						FROM ${table.TABLE_PRODUCT_CATEGORY}
						WHERE parent_category IS NULL and status = 1
						UNION ALL
						SELECT si.product_category_id,si.product_category_name,
							si.parent_category,
							CAST(sp.category_name || '->' || si.product_category_name As varchar(100)) As category_name
						FROM ${table.TABLE_PRODUCT_CATEGORY} As si
							INNER JOIN supplytree AS sp
							ON (si.parent_category = sp.product_category_id) where si.status = 1
						)
						SELECT product_category_id, category_name
						FROM supplytree
						ORDER BY category_name`
			}
			this.db.query(sql).then(res => resolve(res), err => reject(err));
		});
	}

	getServiceCategoriesByParentId(param) {
		return new Promise((resolve, reject) => {
			let sql = '';
			if(param.id){
				sql += `WITH RECURSIVE category_path (id, title, parent_id) AS
						(
						SELECT service_category_id, service_category_name, parent_category
							FROM ${table.TABLE_SERVICE_CATEGORY}
							WHERE service_category_id = ${param.id} and status = 1
						UNION ALL
						SELECT c.service_category_id, c.service_category_name, c.parent_category
							FROM category_path AS cp JOIN ${table.TABLE_SERVICE_CATEGORY} AS c
							ON cp.id = c.parent_category where c.status = 1
						)
						SELECT * FROM category_path`;
			}
			else{
				sql += `WITH RECURSIVE supplytree AS
						(SELECT service_category_id, service_category_name, parent_category, CAST(service_category_name As varchar(100)) As category_name
						FROM ${table.TABLE_SERVICE_CATEGORY}
						WHERE parent_category IS NULL and status = 1
						UNION ALL
						SELECT si.service_category_id,si.service_category_name,
							si.parent_category,
							CAST(sp.category_name || '->' || si.service_category_name As varchar(100)) As category_name
						FROM ${table.TABLE_SERVICE_CATEGORY} As si
							INNER JOIN supplytree AS sp
							ON (si.parent_category = sp.service_category_id) where si.status = 1
						)
						SELECT service_category_id, category_name
						FROM supplytree
						ORDER BY category_name`
			}
			this.db.query(sql).then(res => resolve(res), err => reject(err));
		});
	}


}

module.exports = new webModel();