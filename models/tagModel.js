var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");
const Post = require("./postModel");
const Postfile = require("./postfileModel");

// Setup schema
var tagSchema = mongoose.Schema({
					tagfor: {
						type: String,
						default: "post"
					},
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					postfileid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Postfile
					},
					taggedtext: {
						type: String,
						default: ""
					},
					imageposition: {
						type: String,
						default: ""
					},
					taggeduserid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					},
					status: {
						type: String,
						default: "active"
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export Tag model
var Tag = module.exports = dbconnection.model('tag', tagSchema);

module.exports.get = function (callback, limit) {
    Tag.find(callback).limit(limit);
}