var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');

// Setup schema
var followSchema = mongoose.Schema({
					userid: {
						type: String,
						default: ""
					},
					following_userid: {
						type: String,
						default: ""
					},
					createdon: {
						type: Date,
						default: Date.now
					}
				});

// Export User model
var Follow = module.exports = dbconnection.model('follow', followSchema);

module.exports.get = function (callback, limit) {
    Follow.find(callback).limit(limit);
}