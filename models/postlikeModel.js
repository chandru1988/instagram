var mongoose = require('mongoose');
mongoose.pluralize(null);
const {dbconnection} = require('./../lib/connections');
const User = require("./userModel");
const Post = require("./postModel");

// Setup schema
var postlikeSchema = mongoose.Schema({
					postid: {
						type: mongoose.Schema.Types.ObjectId,
						ref:Post
					},
					liketype: {
						type: String,
						default: "normal" //normal/star/heart
					},
					createdon: {
						type: Date,
						default: Date.now
					},
					createdby: {
						type: mongoose.Schema.Types.ObjectId,
						ref:User
					}
				});

// Export User model
var Postlike = module.exports = dbconnection.model('postlike', postlikeSchema);

module.exports.get = function (callback, limit) {
    Postlike.find(callback).limit(limit);
}