const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.addFollowValidation = function (req, res, next){
    const validationRule = {
		following_userid: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.searchFollowerValidation = function (req, res, next){
    const validationRule = {
		
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}