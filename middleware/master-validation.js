const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.usersValidation = function (req, res, next) {
	const validationRule = {
		username: 'required|email',
		password: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.modelsValidation = function (req, res, next) {
	const validationRule = {
		brand_name: 'required|integer',
		model_code: 'required|string',
		model_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.brandsValidation = function (req, res, next) {
	const validationRule = {
		brand_code: 'required|string',
		brand_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.orgGroupValidation = function (req, res, next) {
	const validationRule = {
		organization_group_code: 'required|string',
		organization_group_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.orgValidation = function (req, res, next) {
	const validationRule = {
		organization_name: 'required|string',
		organization_type: 'required|integer',
		organization_group: 'required|integer',
		email_id: 'required|email',
		address1: 'required|string',
		city: 'required|integer',
		state: 'required|integer',
		country: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.branchValidation = function (req, res, next) {
	const validationRule = {
		branch_code: 'required|string',
		branch_name: 'required|string',
		division_name: 'required|string',
		email_id: 'required|email',
		address1: 'required|string',
		organization_name: 'required|integer',
		city: 'required|integer',
		state: 'required|integer',
		country: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.orgTaxProfileValidation = function (req, res, next) {
	const validationRule = {
		tax_profile_name: 'required|string',
		organization_id: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.divisionValidation = function (req, res, next) {
	const validationRule = {
		organization_name: 'required|integer',
		division_code: 'required|string',
		division_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.sectionValidation = function (req, res, next) {
	const validationRule = {
		department_id: 'required|integer',
		section_code: 'required|string',
		section_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.departmentValidation = function (req, res, next) {
	const validationRule = {
		organization_name: 'required|integer',
		division_name: 'required|integer',
		department_code: 'required|string',
		department_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.uomValidation = function (req, res, next) {
	const validationRule = {
		uom_code: 'required|string',
		uom_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.servicecatValidation = function (req, res, next) {
	const validationRule = {
		service_category_code: 'required|string',
		service_category_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.productcatValidation = function (req, res, next) {
	const validationRule = {
		product_category_code: 'required|string',
		product_category_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.productValidation = function (req, res, next) {
	const validationRule = {
		product_code: 'required|string',
		product_name: 'required|string',
		base_uom: 'required|integer',
		product_category: 'required|integer',
		product_type: 'required|string',
		price: 'required|numeric',
		division: 'required|integer',
		bar_code: 'required|string',
		is_serialized: 'required|boolean',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.productuommapValidation = function (req, res, next) {
	const validationRule = {
		product_id: 'required|integer',
		uom_id: 'required|integer',
		conversion_factor: 'required|string',
		price: 'required|numeric',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.warehouseValidation = function (req, res, next) {
	const validationRule = {
		organization_id: 'required|integer',
		warehouse_code: 'required|string',
		warehouse_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.workshopValidation = function (req, res, next) {
	const validationRule = {
		warehouse_id: 'required|integer',
		workshop_code: 'required|string',
		workshop_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.complaintTypeValidation = function (req, res, next) {
	const validationRule = {
		product_category: 'required|integer',
		complaint_type_code: 'required|string',
		complaint_type_name: 'required|string',
		technician_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.customerClassificationValidation = function (req, res, next) {
	const validationRule = {
		customer_classification_code: 'required|string',
		customer_classification_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.leadSourceValidation = function (req, res, next) {
	const validationRule = {
		source_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.leadTypeValidation = function (req, res, next) {
	const validationRule = {
		lead_type_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.leadCategoryValidation = function (req, res, next) {
	const validationRule = {
		lead_category_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.customerValidation = function (req, res, next) {
	const validationRule = {
		customer_type: 'required|integer',
		customer_code: 'required|string',
		customer_name: 'required|string',
		address1: 'required|string',
		city: 'required|integer',
		state: 'required|integer',
		country: 'required|integer',
		organization_id: 'required|integer',
		email_id: 'required|email',
		phone: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.leadStatusValidation = function (req, res, next) {
	const validationRule = {
		lead_status_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.leadsValidation = function (req, res, next) {
	const validationRule = {
		customer_id: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.opportunitiesValidation = function (req, res, next) {
	const validationRule = {
		lead_id: 'required|integer',
		sales_person_id: 'required|integer',
		customer_id: 'required|integer',
		deadline: 'required|date',
		lead_category: 'required|integer',
		duration: 'required',
		lead_source: 'required|integer',
		lead_status: 'required|integer',
		lead_type: 'required|integer',
		stage: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.reasonValidation = function (req, res, next) {
	const validationRule = {
		reason_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.termsHeaderValidation = function (req, res, next) {
	const validationRule = {
		template_name: 'required|string',
		terms_type: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.termsHeaderLineValidation = function (req, res, next) {
	const validationRule = {
		term_id: 'required|integer',
		milestone_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.quotationHeaderValidation = function (req, res, next) {
	const validationRule = {
		term_id: 'required|integer',
		is_ammended: 'required|string',
		version_number: 'required|integer',
		quotation_number: 'required|string',
		opportunity_id: 'required|integer',
		validity: 'required|date',
		customer_id: 'required|integer',
		stage: 'required|integer',
		quotation_status: 'required|integer',
		gross_amount: 'required|numeric',
		discount_value: 'required|numeric',
		discount: 'required|numeric',
		tax_amount: 'required|numeric',
		net_amount: 'required|numeric',
		approval_status: 'required|integer',
		reason_type_id: 'required|integer',
		term_id: 'required|integer',
		milestone_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.leadActivityValidation = function (req, res, next) {
	const validationRule = {
		lead_id: 'required|integer',
		duration: 'required',
		lead_status: 'required|integer',
		sales_person_id: 'required|integer',
		activity_details: 'required|string',
		activity_type: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.issueTypeValidation = function (req, res, next) {
	const validationRule = {
		type_name: 'required|string',
		complaint_type: 'required|string',
		category_id: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.documentSequenceValidation = function (req, res, next) {
	const validationRule = {
		current_sequence_no: 'required|integer',
		current_year: 'required|integer',
		code_value: 'required|integer',
		module_name: 'required|string',
		user_code: 'required|string',
		prefix_code: 'required|integer',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.customerTypeValidation = function (req, res, next) {
	const validationRule = {
		customer_type: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.activityTypeValidation = function (req, res, next) {
	const validationRule = {
		activity_type: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

exports.departmentValidation = function (req, res, next) {
	const validationRule = {
		department_name: 'required|string',
	};
	/*if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}*/
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};