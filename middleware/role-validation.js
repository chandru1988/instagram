const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.createRole = function (req, res, next) {
	const validationRule = {
		role_name: 'required|string',
		description: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

//createUserRight
exports.createUserRight = function (req, res, next) {
	const validationRule = {
		right_name: 'required|string',
		right_description: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

//createUserRightGroup
exports.createUserRightGroup = function (req, res, next) {
	const validationRule = {
		right_group_name: 'required|string',
		right_group_desc: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};

//createUserMenu
exports.createUserMenu = function (req, res, next) {
	const validationRule = {
		menu_name: 'required|string',
		status: 'required|integer',
	};
	if (req.method == 'POST') {
		validationRule.created_by = 'required|integer';
	}
	if (req.method == 'PUT') {
		validationRule.updated_by = 'required|integer';
	}
	Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res, true, 0, '', err, 'Validation error');
		}
		next();
	});
};
