const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.updateFCMTokenValidation = function (req, res, next){
    const validationRule = {
		userid: 'required|string',
		fcmtoken: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.testFCMMessageValidation = function (req, res, next){
    const validationRule = {
		registration_token: 'required|string',
		notititle: 'required|string',
		notibody: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}