var jwt = require('jsonwebtoken');
var config = require('../config/config');

function verifyToken(req, res, next) {
	console.log(config);
	let authlebel = config.keylabel;
	let tokensecret = config.tokensecret;
	var token = req.headers[authlebel];
	if (!token) return res.status(403).send({ iserror: true, message: 'No token provided.' });

	jwt.verify(token, tokensecret, function (err, decoded) {
		if (err) {
			return res.status(500).send({ iserror: true, message: 'Failed to authenticate token.' });
		}
		req.userid = decoded.userid;
		next();
	});
}

module.exports = verifyToken;
