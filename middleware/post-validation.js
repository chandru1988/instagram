const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.addPostValidation = function (req, res, next){
    const validationRule = {
		message: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.updatePostValidation = function (req, res, next){
    const validationRule = {
		message: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.addLikeValidation = function (req, res, next){
    const validationRule = {
		postid: 'required|string',
		liketype: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.getHashPostListValidation = function (req, res, next){
    const validationRule = {
		hashvalue: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}