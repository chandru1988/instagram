const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.addTagValidation = function (req, res, next){
    const validationRule = {
		postid: 'required|string',
		postfileid: 'required|string',
		taggedtext: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}