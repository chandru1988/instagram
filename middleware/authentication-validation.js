const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.loginValidation = function (req, res, next){
    const validationRule = {
		email: 'required|string',
		password: 'required|string',
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        }
		else
		{
			next();
		}        
	});
}

exports.signupValidation = function (req, res, next){
    const validationRule = {
		email: 'required|string',
		password: 'required|string',
		firstname: 'required|string',
		lastname: 'required|string',
		mobileno: 'required|string',
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        }
		else
		{
			next();
		}        
	});
}

exports.oauthValidation = function (req, res, next){
    const validationRule = {
		oauthid: 'required|string',
		email: 'required|string',
		firstname: 'required|string',
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        }
		else
		{
			next();
		}        
	});
}

exports.activateValidation = function (req, res, next){
    const validationRule = {
		email: 'required|string',
		emailotp: 'required|string',
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        }
		else
		{
			next();
		}        
	});
}

exports.forgotpasswordValidation = function (req, res, next){
    const validationRule = {
		email: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		} 
	});
}

exports.forgotChangePasswordValidation = function (req, res, next){
    const validationRule = {
		email: 'required',
		temp_password: 'required|string',
		new_password: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		} 
	});
}

exports.changePasswordValidation = function (req, res, next){
    const validationRule = {
		old_password: 'required|string',
		new_password: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.updateProfileValidation = function (req, res, next){
    const validationRule = {
		username: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.deviceValidation = function (req, res, next){
    const validationRule = {
		device_no: 'required|string',
		device_imei: 'required|string',
		is_device_locked: 'required|boolean',
		is_pull: 'required|boolean',
		is_lock_initialized: 'required|boolean',
		locked_date_time: 'required|date',
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

