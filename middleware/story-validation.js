const Validator = require('../helpers/validate');
const resmessage = require('../config/response');

exports.addStoryValidation = function (req, res, next){
    const validationRule = {
		filetype: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.updateStoryValidation = function (req, res, next){
    const validationRule = {
		filetype: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.addStoryFromPostValidation = function (req, res, next){
    const validationRule = {
		filetype: 'required|string',
		filepath: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}

exports.addStoryFromPostIdValidation = function (req, res, next){
    const validationRule = {
		filetype: 'required|string',
		postid: 'required|string'
	};
    Validator(req.body, validationRule, {}, (err, status) => {
		if (!status) {
			resmessage.genResponse(res,true,0,'',err,'Validation error');
        } 
        else
		{
			next();
		}
	});
}